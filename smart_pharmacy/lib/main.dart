import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smart_pharmacy/Ui/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Smart Pharmacy',
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
