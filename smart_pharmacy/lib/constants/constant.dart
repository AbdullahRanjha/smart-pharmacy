import 'dart:ui';

import 'package:flutter/material.dart';

class Constants {
  final backgroundColor = Color.fromARGB(255, 226, 251, 254);
  final buttonColor = Color.fromRGBO(53, 162, 159, 1);
  final buttonTextColor = Color.fromARGB(255, 255, 255, 255);
  final textButtonColor = Color.fromRGBO(33, 75, 243, 1);
  final double buttontextFontSize = 23;
  Color blackColor = Colors.black;
  final Color whiteColor = Colors.white;

  // double buttonHeight =MediaQuery.of(context).size.height * 0.06;
//
}
