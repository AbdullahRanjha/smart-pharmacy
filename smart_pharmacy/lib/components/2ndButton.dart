// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

// class SecondButton extends StatelessWidget {
//   String title;
//   Color? textColor, backgroundColor;
//   double height, width, fontSize, radius;
//   VoidCallback onTap;
//   Color? borderColor;

//    SecondButton({super.key

//    required this.title,
//       required this.radius,
//       this.backgroundColor,
//       required this.height,
//       required this.fontSize,
//       required this.onTap,
//       this.textColor,
//       required this.width

//    });

//   @override
//   Widget build(BuildContext context) {
//     return
//     InkWell(
//       onTap: onTap,
//       child: Padding(
//         padding: const EdgeInsets.all(10.0),
//         child: Container(
//           height: height,
//           width: width,
//           decoration: BoxDecoration(
//               // border: Border.all(),
//               borderRadius: BorderRadius.circular(radius),
//               color: backgroundColor),
//           child: Center(
//             child: Text(import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/constants/constant.dart';
import 'package:flutter/material.dart';

class SecondButton extends StatelessWidget {
  String title;
  Color? textColor, backgroundColor;
  double height, width, fontSize, radius;
  VoidCallback onTap;
  Color borderColor;

  SecondButton(
      {super.key,
      required this.title,
      required this.radius,
      required this.borderColor,
      this.backgroundColor,
      required this.height,
      required this.fontSize,
      required this.onTap,
      this.textColor,
      required this.width});
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
              border: Border.all(color: borderColor),
              borderRadius: BorderRadius.circular(radius),
              color: backgroundColor),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                fontSize: fontSize,
                color: textColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//               title,
//               style: TextStyle(
//                   fontSize: fontSize,
//                   color: textColor,
//                   fontWeight: FontWeight.bold),
//             ),
//           ),
//         ),
//       ),
//     )
//     ;
//   }
// }
