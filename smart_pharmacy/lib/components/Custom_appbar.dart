import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomAppbar extends StatelessWidget {
  const CustomAppbar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      child: Row(
        children: [
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/splashPicture.jpg'))),
          ),
          Container(
            child: Text('Smart Pharmacy',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal)),
          )
        ],
      ),
    );
  }
}
