import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyTextButton extends StatelessWidget {
  VoidCallback onTap;
  String title;
  Color color;

   MyTextButton({super.key, required this.color, required this.onTap, required this.title});

  @override
  Widget build(BuildContext context) {
    return   InkWell(
                    onTap: onTap,
                    child: Container(
                        child: Text(title,
                            style: TextStyle(
                                color:
                                   color))));
  }
}