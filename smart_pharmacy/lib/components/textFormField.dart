import 'package:flutter/material.dart';

class CustomtextFormField extends StatelessWidget {
  String? lableText;
  String hintText;
  double radius;
  double verticalPadding;
  double horizontalPadding;
  dynamic controller;
  // double textFieldHeight = 0.0;
  dynamic keyBoardtype;
  bool? filled;
  String? suffixText;
  Color? fillColor, focusColor, labelColor;

  Widget? suffix;
  Widget? prefix;

  CustomtextFormField(
      {super.key,
      required this.hintText,
      required this.horizontalPadding,
      required this.keyBoardtype,
      this.filled,
      this.labelColor,
      this.suffix,
      this.suffixText,
      this.prefix,
      this.fillColor,
      this.focusColor,
      this.lableText,
      required this.controller,
      required this.radius,
      // required this.textFieldHeight,
      required this.verticalPadding});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: TextFormField(
        keyboardType: keyBoardtype,
        decoration: InputDecoration(
            fillColor: fillColor,
            prefixIcon: prefix,
            suffix: suffix,
            labelStyle: TextStyle(color: labelColor),
            filled: filled,
            focusColor: focusColor,
            // contentPadding: EdgeInsets.symmetric(vertical: textFieldHeight),
            hintText: hintText,
            label: Text(lableText!),
            focusedBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(radius)),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius))),
        controller: controller,
      ),
    );
  }
}
