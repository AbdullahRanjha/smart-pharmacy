import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class CustomButton extends StatelessWidget {
  String title;
  Color? textColor, backgroundColor;
  double height, width, fontSize, radius;
  VoidCallback onTap;
  Color? borderColor;

  CustomButton(
      {super.key,
      required this.title,
      required this.radius,
      this.backgroundColor,
      required this.height,
      required this.fontSize,
      required this.onTap,
      this.textColor,
      required this.width});
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
              // border: Border.all(),
              borderRadius: BorderRadius.circular(radius),
              color: backgroundColor),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                fontSize: fontSize,
                color: textColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
