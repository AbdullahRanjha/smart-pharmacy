class Products {
  String name;
  int productId;
  String url;
  double price;

  Products(
      {required this.name,
      required this.price,
      required this.productId,
      required this.url});

  static List<Products> productsList = [
    Products(
        name: 'Product 1',
        price: 350,
        productId: 1,
        url:
            'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/376202403_3161993377439816_9075350442434924379_n.jpg?stp=cp0_dst-jpg&_nc_cat=105&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeFn_fpWU1rMC0jHUA406WYXSH2W9r2ceLtIfZb2vZx4u2bgtHI8RvDum--4RmomoCl3Cx4gUucRwlXbj5bRddtg&_nc_ohc=1AqAm0iZPGUAX_viQUj&_nc_ht=scontent.fskt5-1.fna&oh=00_AfAXQiOCo6jlYymTZLw0fYLmFgDJQNhMBT2CnQ8hLAZXNw&oe=6503C6CE'),
    Products(
        name: 'Product 2',
        price: 150,
        productId: 2,
        url:
            'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/376879233_3161993374106483_1178683762241734094_n.jpg?stp=cp0_dst-jpg&_nc_cat=109&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeHeww9TrhblggvmMgtvaKNRK3Q03VqY60wrdDTdWpjrTCAZBzLIOW8SRYjSFGUQmuJFHiHfc0oLwAjGrVUNwVyl&_nc_ohc=iPWm7M4z0k0AX8VV_i4&_nc_ht=scontent.fskt5-1.fna&oh=00_AfBXXF7Y9i9si_vLGyABX_OsvRHB6TlCK1FLtJ5x8Roalg&oe=6503405B'),
    Products(
        name: 'Product 3',
        price: 220,
        productId: 3,
        url:
            'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/374141356_3161993380773149_4521499950997201894_n.jpg?stp=cp0_dst-jpg&_nc_cat=103&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeGBH8FqRr48bUV7usL2ksa1Plk1cvH9Mpw-WTVy8f0ynIrNxc3rpkc0reBO4TMt4lYT5zkNOFukcHm7SK3iiSyW&_nc_ohc=zUGORzDT6TAAX8VN-93&_nc_ht=scontent.fskt5-1.fna&oh=00_AfBeZj56w3aGeqEXmF_BT542xSm8rm3wjh2rCI4nHPQapw&oe=6502F76F'),
    Products(
        name: 'Product 4',
        price: 100,
        productId: 4,
        // url:
        url:
            'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/376879233_3161993374106483_1178683762241734094_n.jpg?stp=cp0_dst-jpg&_nc_cat=109&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeHeww9TrhblggvmMgtvaKNRK3Q03VqY60wrdDTdWpjrTCAZBzLIOW8SRYjSFGUQmuJFHiHfc0oLwAjGrVUNwVyl&_nc_ohc=iPWm7M4z0k0AX8VV_i4&_nc_ht=scontent.fskt5-1.fna&oh=00_AfBXXF7Y9i9si_vLGyABX_OsvRHB6TlCK1FLtJ5x8Roalg&oe=6503405B'),
    Products(
        name: 'Product 5',
        price: 230,
        productId: 5,
        // url:

        url:
            'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/376202403_3161993377439816_9075350442434924379_n.jpg?stp=cp0_dst-jpg&_nc_cat=105&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeFn_fpWU1rMC0jHUA406WYXSH2W9r2ceLtIfZb2vZx4u2bgtHI8RvDum--4RmomoCl3Cx4gUucRwlXbj5bRddtg&_nc_ohc=1AqAm0iZPGUAX_viQUj&_nc_ht=scontent.fskt5-1.fna&oh=00_AfAXQiOCo6jlYymTZLw0fYLmFgDJQNhMBT2CnQ8hLAZXNw&oe=6503C6CE'),
    Products(
        name: 'Product 6',
        price: 400,
        productId: 6,
        url:
            'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/374141356_3161993380773149_4521499950997201894_n.jpg?stp=cp0_dst-jpg&_nc_cat=103&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeGBH8FqRr48bUV7usL2ksa1Plk1cvH9Mpw-WTVy8f0ynIrNxc3rpkc0reBO4TMt4lYT5zkNOFukcHm7SK3iiSyW&_nc_ohc=zUGORzDT6TAAX8VN-93&_nc_ht=scontent.fskt5-1.fna&oh=00_AfBeZj56w3aGeqEXmF_BT542xSm8rm3wjh2rCI4nHPQapw&oe=6502F76F'),

    // url:
  ];
}

class CartItems {
  // final Products products;
  int price;
  String url;
  double? totalPrice;
  String name;
  int productId;
  int Quantity;

  CartItems(
      {required this.name,
      required this.price,
      required this.Quantity,
      this.totalPrice,
      required this.productId,
      required this.url});
  static List<CartItems> cartItems = [];
  // int count;
  // static totalQuantity(int index) {
  //   CartItems.cartItems[index].Quantity = CartItems.cartItems[index].Quantity++;
  //   // setState(() {});
  //   print(
  //       "Your count value is = ${CartItems.cartItems[index].Quantity.toString()}");
  // }
}
