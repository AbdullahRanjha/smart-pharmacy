import 'package:flutter/material.dart';
import 'package:smart_pharmacy/Ui/screens/signup/buyer_signup.dart';
import 'package:smart_pharmacy/Ui/screens/signup/seller_signup.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class Buyer_and_Seller_signup extends StatefulWidget {
  const Buyer_and_Seller_signup({super.key});

  @override
  State<Buyer_and_Seller_signup> createState() =>
      _Buyer_and_Seller_signupState();
}

class _Buyer_and_Seller_signupState extends State<Buyer_and_Seller_signup> {
  Constants constants = Constants();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: constants.backgroundColor,
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              // SizedBox(
              //   height: 30,
              // ),
              Image.asset('assets/1stPicture.jpg'),
              SizedBox(
                height: 100,
              ),
              CustomButton(
                  title: "Signup as Buyer",
                  radius: 10,
                  backgroundColor: constants.buttonColor,
                  height: MediaQuery.of(context).size.height * 0.06,
                  fontSize: 25,
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => BuyerSignup()));
                  },
                  textColor: constants.whiteColor,
                  width: MediaQuery.of(context).size.width * 0.79),
              SizedBox(
                height: 10,
              ),
              CustomButton(
                  title: "Signup as Seller",
                  radius: 10,
                  backgroundColor: constants.buttonColor,
                  height: MediaQuery.of(context).size.height * 0.06,
                  fontSize: 25,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SellerSignup()));
                  },
                  textColor: constants.whiteColor,
                  width: MediaQuery.of(context).size.width * 0.79)
            ],
          ),
        ),
      ),
    );
  }
}
