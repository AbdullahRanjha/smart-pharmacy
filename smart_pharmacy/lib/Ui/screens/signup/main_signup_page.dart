import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/main_loginPage.dart';
import 'package:smart_pharmacy/Ui/screens/signup/buyer_and_seller_signup.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textbuttom.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({super.key});

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  Constants constants = Constants();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        // backgroundColor: constants.backgroundColor,
        // backgroundColor: Color.fromARGB(255, 221, 223, 233),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Image.asset(
                    'assets/3rdPicture.jpg',
                    fit: BoxFit.fill,
                    height: MediaQuery.of(context).size.height * 0.4,
                  ),
                ),
                SizedBox(
                  height: 80,
                ),
                Container(
                  child: Center(
                    child: Text(
                      'Deliver at door steps /',
                      style: TextStyle(
                        fontSize: 23,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Center(
                    child: Text(
                      'Collect from nearest',
                      style: TextStyle(fontSize: 23),
                    ),
                  ),
                ),
                Container(
                  child: Center(
                    child: Text(
                      'medical store',
                      style: TextStyle(fontSize: 23),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                CustomButton(
                    title: 'Signup',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: constants.buttontextFontSize,
                    onTap: () {
                      Get.to(Buyer_and_Seller_signup());
                    },
                    textColor: constants.buttonTextColor,
                    width: MediaQuery.of(context).size.width * 0.79),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Already have an account? ',
                      style: TextStyle(fontSize: 15),
                    ),
                    MyTextButton(
                        color: constants.textButtonColor,
                        onTap: () {
                          Get.to(MainLoginPage());
                        },
                        title: 'Login')
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Use a guest ', style: TextStyle(fontSize: 15)),
                    MyTextButton(
                        color: constants.textButtonColor,
                        onTap: () {},
                        title: 'Guest')
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
