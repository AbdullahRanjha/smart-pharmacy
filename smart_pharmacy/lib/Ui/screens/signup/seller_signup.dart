import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class SellerSignup extends StatefulWidget {
  const SellerSignup({super.key});

  @override
  State<SellerSignup> createState() => _SellerSignupState();
}

class _SellerSignupState extends State<SellerSignup> {
  Constants constants = Constants();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Image.asset(
                    'assets/6thPharmacy.jpg',
                    height: MediaQuery.of(context).size.height * 0.32,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Signup Seller',
                  style: GoogleFonts.abhayaLibre(fontSize: 30),
                ),
                SizedBox(
                  height: 10,
                ),
                CustomtextFormField(
                    hintText: 'Enter Email',
                    horizontalPadding: 10,
                      // textFieldHeight: 0,
                        keyBoardtype: TextInputType.emailAddress,
                    lableText: 'Email',
                    controller: emailController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Enter Password',
                    horizontalPadding: 10,
                      // textFieldHeight: 0,
                    lableText: 'Password',
                      keyBoardtype: TextInputType.emailAddress,
                    controller: passwordController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Confirm Password',
                    horizontalPadding: 10,
                      // textFieldHeight: 0,
                        keyBoardtype: TextInputType.emailAddress,
                    lableText: 'Confirm Password',
                    controller: confirmPasswordController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Enter Phone Number',
                    horizontalPadding: 10,
                    lableText: 'Phone',
                      keyBoardtype: TextInputType.emailAddress,
                      // textFieldHeight: 0,
                    controller: phoneNoController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Enter Home Address',
                    horizontalPadding: 10,
                    lableText: 'Address',
                    keyBoardtype: TextInputType.emailAddress,
                    controller: addressController,
                      // textFieldHeight: 0,
                    radius: 10,
                    verticalPadding: 10),
                SizedBox(
                  height: 10,
                ),
                CustomButton(
                    title: 'Signup',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: 25,
                    onTap: () {},
                    textColor: constants.buttonTextColor,
                    width: MediaQuery.of(context).size.width * 0.95),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
