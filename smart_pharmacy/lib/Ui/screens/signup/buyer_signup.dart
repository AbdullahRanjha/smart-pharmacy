import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class BuyerSignup extends StatefulWidget {
  const BuyerSignup({super.key});

  @override
  State<BuyerSignup> createState() => _BuyerSignupState();
}

class _BuyerSignupState extends State<BuyerSignup> {
  Constants constants = Constants();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: constants.backgroundColor,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Image.asset(
                  'assets/8thPicture.jpg',
                  height: MediaQuery.of(context).size.height * 0.32,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Buyer Signup',
                  style: GoogleFonts.abhayaLibre(fontSize: 30),
                ),
                SizedBox(
                  height: 10,
                ),
                CustomtextFormField(
                    hintText: 'Enter Email',
                      keyBoardtype: TextInputType.emailAddress,
                    horizontalPadding: 10,
                    lableText: 'Email',
                      
                    controller: emailController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Enter Password',
                      keyBoardtype: TextInputType.emailAddress,
                    horizontalPadding: 10,
                       
                    lableText: 'Password',
                    controller: passwordController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Confirm Password',
                      keyBoardtype: TextInputType.emailAddress,
                       
                    horizontalPadding: 10,
                    lableText: 'Confirm Password',
                    controller: confirmPasswordController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Enter Phone Number',
                      keyBoardtype: TextInputType.emailAddress,
                       
                    horizontalPadding: 10,
                    lableText: 'Phone',
                    controller: phoneNoController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    keyBoardtype: TextInputType.emailAddress,
                    hintText: 'Enter Home Address',
                    horizontalPadding: 10,
                    lableText: 'Address',
                      
                    controller: addressController,
                    radius: 10,
                    verticalPadding: 10),
                // SizedBox(
                //   height: 10,
                // ),
                CustomButton(
                    title: 'Signup',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    fontSize: 25,
                    onTap: () {},
                    textColor: constants.buttonTextColor,
                    height: MediaQuery.of(context).size.height * 0.06,

                    width: MediaQuery.of(context).size.width * 0.95
                    )
                // Container(
                //   padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                //   child: TextFormField(
                //     decoration: InputDecoration(
                //         hintText: 'Enter Email',
                //         label: Text('Email'),
                //         focusedBorder: OutlineInputBorder(
                //             borderRadius: BorderRadius.circular(10)),
                //         border: OutlineInputBorder(
                //             borderRadius: BorderRadius.circular(10))),
                //     controller: emailController,
                //   ),
                // )
               
              ],
            ),
          ),
        ),
      ),
    );
  }
}
