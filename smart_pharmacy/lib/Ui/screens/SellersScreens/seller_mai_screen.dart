// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/cart_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/dashboard_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/main_page.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/preception_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/SellersScreens/Seller_menu_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/SellersScreens/seller_dashboard_Screen.dart';
// import 'package:smart_pharmacy/Ui/screens/SellersScreens/order_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/SellersScreens/products_screen.dart';
// import 'package:smart_pharmacy/constants/constant.dart';

// class MainScreen extends StatefulWidget {
//   const MainScreen({super.key});

//   @override
//   State<MainScreen> createState() => _MainScreenState();
// }

// class _MainScreenState extends State<MainScreen> {
//   Constants constants = Constants();
//   @override
//   Widget build(BuildContext context) {
//     return CupertinoTabScaffold(
//         tabBar: CupertinoTabBar(
//           activeColor: Colors.blue,
//           inactiveColor: Colors.black,
//           items: <BottomNavigationBarItem>[
//             BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.home,
//                 size: 30,
//                 color: constants.blackColor,
//               ),
//               label: 'Home',
//             ),
//             BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.upload_file,
//                 color: constants.blackColor,
//                 size: 30,
//               ),
//               label: 'Preceptions',
//             ),
//             BottomNavigationBarItem(
//               icon: Badge(
//                 label: Text(
//                   '0',
//                   style: TextStyle(color: Colors.white),
//                 ),
//                 child: Icon(
//                   Icons.shopping_cart,
//                   color: constants.blackColor,
//                   size: 30,
//                 ),
//               ),
//               label: 'Cart',
//             ),
//             BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.menu,
//                 size: 30,
//               ),
//               label: 'Menu',
//             )

//             // BottomNavigationBarItem(icon: )
//           ],
//         ),
//         tabBuilder: (BuildContext context, index) {
//           switch (index) {
//             case 0:
//               return CupertinoTabView(
//                 builder: (context) {
//                   return CupertinoPageScaffold(
//                       child: SafeArea(child: RenameDashboardScreen()));
//                 },
//               );
//             case 1:
//               return CupertinoTabView(
//                 builder: (context) {
//                   return CupertinoPageScaffold(child: OrderScreen());
//                 },
//               );
//             case 2:
//               return CupertinoTabView(
//                 builder: (context) {
//                   return CupertinoPageScaffold(child: ProductsScreen());
//                 },
//               );
//             case 3:
//               return CupertinoTabView(
//                 builder: (context) {
//                   return CupertinoPageScaffold(child: MenuScreen());
//                 },
//               );
//             // break;
//             // default:
//           }
//           return Container();
//         });
//   }
// }
