import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class ProductDetailsScreen extends StatefulWidget {
  const ProductDetailsScreen({super.key});

  @override
  State<ProductDetailsScreen> createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  TextEditingController productNameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController productPriceController = TextEditingController();
  TextEditingController catagoryController = TextEditingController();
  String selectedCatagory = 'Select catagory';
  bool isOpen = false;
  List<String> dropDownValues = ['Kidney', 'Lungs', 'Eyes'];
  Constants constants = Constants();
  final imagePicker = ImagePicker();
  String imagePath = '';
  pickImage() async {
    final pickImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickImage != null) {
      imagePath = pickImage.path;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: CustomButton(
            title: 'Add Product',
            radius: 10,
            textColor: Colors.white,
            height: 45,
            fontSize: 20,
            backgroundColor: constants.buttonColor,
            onTap: () {
              Fluttertoast.showToast(
                  msg: 'Product added',
                  gravity: ToastGravity.BOTTOM,
                  backgroundColor: Colors.black);
              // pickImage();
              // setState(() {});
            },
            width: double.infinity),
      ),
      appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: true,
          title: Text(
            'Add Product Screen',
            style: TextStyle(fontSize: 21, fontWeight: FontWeight.normal),
          )),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Container(
                  alignment: Alignment.topLeft,
                  child: Text('Add New Products',
                      style: TextStyle(
                        fontSize: 19,
                      ))),
              // SizedBox(
              //   height: 5,
              // ),
              CustomtextFormField(
                  hintText: 'Product Name',
                  horizontalPadding: 0,
                  keyBoardtype: TextInputType.emailAddress,
                  lableText: 'Product Name',
                  controller: productNameController,
                  radius: 10,
                  // textFieldHeight: 15,
                  verticalPadding: 0),

              CustomtextFormField(
                  hintText: 'Product Description',
                  horizontalPadding: 0,
                  keyBoardtype: TextInputType.emailAddress,
                  lableText: 'Product Decription',
                  controller: productNameController,
                  radius: 10,
                  // textFieldHeight: 15,
                  verticalPadding: 0),

              CustomtextFormField(
                  hintText: 'Product Price',
                  horizontalPadding: 0,
                  keyBoardtype: TextInputType.emailAddress,
                  lableText: 'Product Price',
                  controller: productNameController,
                  radius: 10,
                  // textFieldHeight: 15,
                  verticalPadding: 0),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  isOpen = !isOpen;
                  setState(() {});
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: constants.buttonColor,
                      borderRadius: BorderRadius.circular(10)),
                  width: double.infinity,
                  height: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '${selectedCatagory.toString()}',
                          style: TextStyle(
                              color: constants.whiteColor, fontSize: 16),
                        ),
                        Icon(
                          isOpen ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                          color: constants.whiteColor,
                          size: 35,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              if (isOpen == true)
                ListView(
                  shrinkWrap: true,
                  children: dropDownValues
                      .map((e) => Container(
                            decoration: BoxDecoration(
                                // borderRadius: BorderR,
                                color: Colors.grey[300]),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 10.0, bottom: 10),
                              child: InkWell(
                                  onTap: () {
                                    selectedCatagory = e.toString();
                                    isOpen = !isOpen;
                                    setState(() {});
                                  },
                                  child: Text(e.toString())),
                            ),
                          ))
                      .toList(),
                ),
              SizedBox(height: 10),
              InkWell(
                onTap: () {
                  pickImage();
                  setState(() {});
                },
                child: Container(
                  height: 220,
                  width: 230,
                  // child: Text(),

                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.grey),
                      image: DecorationImage(
                          image: FileImage(File(imagePath.toString())),
                          fit: BoxFit.fill)),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              // CustomButton(
              //     title: 'Add Product',
              //     radius: 10,
              //     textColor: Colors.white,
              //     height: 45,
              //     fontSize: 20,
              //     backgroundColor: constants.buttonColor,
              //     onTap: () {
              //       Fluttertoast.showToast(
              //           msg: 'Product added',
              //           gravity: ToastGravity.BOTTOM,
              //           backgroundColor: Colors.black);
              //       // pickImage();
              //       // setState(() {});
              //     },
              //     width: double.infinity)
            ],
          ),
        ),
      ),
    );
  }
}
