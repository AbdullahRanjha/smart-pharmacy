import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class SellerProfileScreen extends StatefulWidget {
  const SellerProfileScreen({super.key});

  @override
  State<SellerProfileScreen> createState() => _SellerProfileScreenState();
}

class _SellerProfileScreenState extends State<SellerProfileScreen> {
  Constants constants = Constants();


  final ImagePicker picker = ImagePicker();
  TextEditingController nameController = TextEditingController();
  // TextEditingController phoneController = TextEditingController();
  // TextEditingController addressController = TextEditingController();
  // TextEditingController emailController = TextEditingController();

  List<String> subtitle = [
    'Abdullah Khan',
    '+923001345678',
    'seller123@gmail.com',
    'sellerAddress G-2 block Islamabad',
    'sellerPasswrord'
  ];
  List<String> title = ['Name', 'Phone', 'Address', 'Email', 'Password'];
  List<IconData> iconsList = [
    CupertinoIcons.person,
    CupertinoIcons.phone,
    CupertinoIcons.location,
    CupertinoIcons.mail,
    CupertinoIcons.lock,
  ];
  String imagePath = '';

  getImageFromGallery() async {
    final image = await picker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      imagePath = image.path.toString();
    }
    // ;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // elevation: 500,
        // shadowColor: constants.buttonColor,
        centerTitle: true,
        title: Text(
          'Seller Profile',
          style: TextStyle(fontSize: 22),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          child: ListView(
            children: [
              SizedBox(
                height: 5,
              ),
              Center(
                child: Stack(
                  children: [
                    Card(
                      elevation: 10,
                      // color: constants.,
                      shadowColor: constants.buttonColor,
                      borderOnForeground: true,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(68)),
                      child: CircleAvatar(
                        radius: 70,
                        foregroundColor: Colors.green,
                        backgroundImage: FileImage(File(imagePath.toString())),
                      ),
                    ),
                    Positioned(
                      // top: 0,
                      // top: 100,
                      bottom: 0,
                      right: 15,
                      child: InkWell(
                        onTap: () => getImageFromGallery(),
                        child: Container(
                          height: 35,
                          width: 35,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.white),
                              color: constants.buttonColor),
                          child: Icon(
                            Icons.edit,
                            color: constants.whiteColor,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(boxShadow: [
                  // BoxShadow(
                  //     offset: Offset(0, 5),
                  //     blurRadius: 10,
                  //     spreadRadius: 9,
                  //     color: constants.buttonColor.withOpacity(0.2))
                ]),
                height: MediaQuery.of(context).size.height * 0.51,
                child: ListView.builder(
                    itemCount: 5,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          // height: MediaQuery.of(context).size.height * 0.5,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 5),
                                    blurRadius: 10,
                                    spreadRadius: 9,
                                    blurStyle: BlurStyle.normal,
                                    color:
                                        constants.buttonColor.withOpacity(0.2))
                              ]),
                          // color: Colors.red,
                          child: Column(
                            children: [
                              ListTile(
                                title: Text('${title[index].toString()}'),
                                subtitle: Text('${subtitle[index].toString()}'),
                                leading: Icon(iconsList[index]),
                                trailing: InkWell(
                                    onTap: () {
                                      Get.defaultDialog(
                                          textConfirm: 'save',
                                          textCancel: 'cancel',
                                          onCancel: () {
                                            setState(() {});
                                            Get.back();
                                          },
                                          onConfirm: () {
                                            subtitle[index] =
                                                nameController.text;
                                            setState(() {});
                                            // Get.back();
                                            Navigator.pop(context);
                                          },
                                          title: '${title[index]}',
                                          content: Column(
                                            children: [
                                              TextFormField(
                                                decoration: InputDecoration(
                                                  hintText:
                                                      title[index].toString(),
                                                  label: Text(
                                                      title[index].toString()),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10)),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                ),
                                                controller: nameController,
                                              ),
                                            ],
                                          ));
                                    },
                                    child: Icon(CupertinoIcons.pencil)),
                              ),
                              // title.toSet()
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              SizedBox(
                height: 15,
              ),
              CustomButton(
                  title: 'Save',
                  radius: 10,
                  height: 45,
                  fontSize: 22,
                  textColor: constants.whiteColor,
                  backgroundColor: constants.buttonColor,
                  onTap: () {
                    Fluttertoast.showToast(
                        textColor: constants.whiteColor,
                        gravity: ToastGravity.BOTTOM,
                        msg: 'Save changes');
                  },
                  width: double.infinity)
            ],
          ),
        ),
      ),
    );
  }
}
