import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_offer_screen1.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/cart_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/dashboard_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/main_page.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/preception_screen.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/Seller_menu_screen.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/help_&_supportPage.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/sellerAccountStatement.dart';
// import 'package:smart_pharmacy/Ui/screens/SellersScreens/seller_profile_screen.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/seller_dashboard_Screen.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/order_screen.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/products_screen.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/seller_settings.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/main_loginPage.dart';
import 'package:smart_pharmacy/components/2ndButton.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/constants/constant.dart';

import 'seller_profile_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // void _onMenuTap() {
  //   _scaffoldKey.currentState?.openDrawer();
  // }

  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          backgroundColor: Colors.white,
          activeColor: Colors.blue,
          // onTap: (v) {},
          // onTap: (value) => _onMenuTap(),
          inactiveColor: Colors.black,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/home.jpg',
                fit: BoxFit.fill,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/delivery.jpg',
                fit: BoxFit.fill,
              ),
              label: 'Orders',
            ),
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/product.jpg',
                fit: BoxFit.fill,
              ),
              label: 'Products',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.menu,
                size: 30,
              ),
              label: 'Menu',
            )

            // BottomNavigationBarItem(icon: )
          ],
        ),
        tabBuilder: (BuildContext context, index) {
          switch (index) {
            case 0:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(
                      child: SafeArea(child: RenameDashboardScreen()));
                },
              );
            case 1:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: OrderScreen());
                },
              );
            case 2:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: ProductsScreen());
                },
              );
            case 3:
              return CupertinoTabView(
                builder: (context) {
                  return SafeArea(
                    child: Scaffold(
                      // key: _scaffoldKey,
                      body: Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: double.infinity,
                            width: MediaQuery.of(context).size.width * 0.35,
                            color: Colors.grey[400],
                          ),
                          Container(
                            // clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              color: constants.buttonColor,
                              // borderRadius: BorderRadius.only(
                              //     topLeft: Radius.circular(10),
                              //     bottomLeft: Radius.circular(10))
                            ),
                            height: double.infinity,
                            width: MediaQuery.of(context).size.width * 0.65,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: ListView(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        child: CircleAvatar(
                                          radius: 45,
                                          child: Text(
                                            'AR',
                                            style: TextStyle(fontSize: 27),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          // mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Ali Raza',
                                              style: TextStyle(
                                                  fontSize: 22,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'User02',
                                              style: TextStyle(fontSize: 17),
                                            ),
                                          ],
                                        ),
                                      )
                                      // Column()
                                    ],
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Column(
                                    // crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        child: Column(
                                          children: [
                                            SecondButton(
                                                title: 'Profile',
                                                radius: 0,
                                                borderColor: Colors.white,
                                                height: 40,
                                                textColor: Colors.white,
                                                fontSize: 17,
                                                onTap: () {
                                                  Get.to(SellerProfileScreen());
                                                },
                                                width: double.infinity),
                                            // SecondButton(
                                            //     title: 'Seller Account',
                                            //     radius: 0,
                                            //     borderColor: Colors.white,
                                            //     height: 40,
                                            //     textColor: Colors.white,
                                            //     fontSize: 20,
                                            //     onTap: () {},
                                            //     width: double.infinity),
                                            SecondButton(
                                                title: 'Account Statement',
                                                radius: 0,
                                                borderColor: Colors.white,
                                                height: 40,
                                                textColor: Colors.white,
                                                fontSize: 20,
                                                onTap: () {
                                                  Get.to(
                                                      SellerAccountStatement());
                                                },
                                                width: double.infinity),
                                            // SecondButton(
                                            //     title: 'Payment',
                                            //     radius: 0,
                                            //     borderColor: Colors.white,
                                            //     height: 40,
                                            //     textColor: Colors.white,
                                            //     fontSize: 20,
                                            //     onTap: () {},
                                            //     width: double.infinity),

                                            SecondButton(
                                                title: 'Offers',
                                                radius: 0,
                                                borderColor: Colors.white,
                                                height: 40,
                                                textColor: Colors.white,
                                                fontSize: 20,
                                                onTap: () {
                                                  Get.to(BuyerOfferScreen());
                                                },
                                                width: double.infinity),
                                            SecondButton(
                                                title: 'Settings',
                                                radius: 0,
                                                borderColor: Colors.white,
                                                height: 40,
                                                textColor: Colors.white,
                                                fontSize: 20,
                                                onTap: () {
                                                  Get.to(SellerProfileScreen());
                                                },
                                                width: double.infinity),
                                            SecondButton(
                                                title: 'Help & Supports',
                                                radius: 0,
                                                borderColor: Colors.white,
                                                height: 40,
                                                textColor: Colors.white,
                                                fontSize: 20,
                                                onTap: () {
                                                  Get.to(
                                                      SellerHelpSupportScreen());
                                                },
                                                width: double.infinity),
                                            // SecondButton(
                                            //     title: 'Settings',
                                            //     radius: 0,
                                            //     borderColor: Colors.white,
                                            //     height: 40,
                                            //     textColor: Colors.white,
                                            //     fontSize: 20,
                                            //     onTap: () {},
                                            //     width: double.infinity),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 230),
                                        // alignment:,
                                        child: SecondButton(
                                            title: 'Logout',
                                            radius: 0,
                                            borderColor: Colors.white,
                                            height: 40,
                                            textColor: Colors.white,
                                            fontSize: 20,
                                            onTap: () {
                                              Get.to(MainLoginPage());
                                            },
                                            width: double.infinity),
                                      )
                                    ],
                                  )
                                  // DrawerHeader(
                                  //     decoration: BoxDecoration(color: Colors.grey[350]),
                                  //     child: Column(
                                  //       children: [
                                  //         Container(
                                  //           child: Text('Name'),
                                  //         ),
                                  //         Container(
                                  //           child: Text('Name'),
                                  //         ),
                                  //         Container(
                                  //           child: Text('Name'),
                                  //         ),
                                  //         Container(
                                  //           child: Text('Name'),
                                  //         )
                                  //       ],
                                  //     ))
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),

                      // appBar: AppBar(
                      //   automaticallyImplyLeading: true,
                      //   title: Row(
                      //     children: [
                      //       Container(
                      //         height: 50,
                      //         width: 50,
                      //         decoration: BoxDecoration(
                      //             image: DecorationImage(
                      //                 image: AssetImage('assets/splashPicture.jpg'),
                      //                 fit: BoxFit.fill)),
                      //       ),
                      //       Text(
                      //         'Smart Pharmacy',
                      //         style: GoogleFonts.abhayaLibre(
                      //             fontSize: 25, fontWeight: FontWeight.normal),
                      //       )
                      //     ],
                      //   ),
                      // ),
                      // child: Padding(
                      //   padding: const EdgeInsets.all(40.0),
                      //   child: SingleChildScrollView(
                      //     child: Column(
                      //       children: [
                      //         CustomAppbar(),
                      //         SizedBox(
                      //           height: 10,
                      //         ),
                      //         Center(
                      //           child: Container(
                      //             child: Text('Swipe left to show menu'),
                      //           ),
                      //         ),
                      //         Center(
                      //           child: Container(
                      //             child: Icon(
                      //               Icons.arrow_back,
                      //               size: 60,
                      //             ),
                      //           ),
                      //         )
                      //       ],
                      //     ),
                      //   ),
                      // ),
                      // endDrawer: Drawer(
                      //   child: ListView(
                      //     children: [
                      //       DrawerHeader(
                      //         child: Text('Drawer Header'),
                      //       ),
                      //       ListTile(
                      //         title: Text('Item 1'),
                      //         onTap: () {},
                      //       ),
                      //       ListTile(
                      //         title: Text('Item 2'),
                      //         onTap: () {},
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // body: CupertinoPageScaffold(
                      //   child: MenuScreen(),
                      // ),
                    ),
                  );
                },
              );
            // break;
            // default:
          }
          return Container();
        });
  }
}
