// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/textFormField.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({super.key});

  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  TextEditingController searchController = TextEditingController();
  List<Color> colorsList = [
    // Colors.yellow,
    Colors.purple,
    Colors.red,
    Color.fromARGB(255, 202, 187, 49),
    Colors.purple,
    Colors.red,
    // Colors.yellow,
    Color.fromARGB(255, 202, 187, 49),

    Colors.purple,
    Colors.red,
    Colors.red,
    // Colors.yellow,
    Color.fromARGB(255, 202, 187, 49),
  ];
  List<String> orderStatus = [
    'Completed',
    'Pending',
    'Cancel',
    'Completed',
    'Pending',
    'Cancel',
    'Completed',
    'Pending',
    'Cancel',
    'Complete'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/splashPicture.jpg'),
                      fit: BoxFit.fill)),
            ),
            Text(
              'Smart Pharmacy',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              // SizedBox(
              //   height: 10,
              // ),

              CustomtextFormField(
                  hintText: 'Search',
                  horizontalPadding: 0,
                  keyBoardtype: TextInputType.emailAddress,
                  lableText: 'Search',
                  controller: searchController,
                  radius: 10,
                  prefix: Icon(Icons.search),
                  // textFieldHeight: 15,
                  verticalPadding: 0),

              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      'All Orders',
                      style: TextStyle(color: Colors.grey, fontSize: 15),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      'Pending',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      'Complete',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    ),
                  ),
                ],
              ),
              Divider(
                color: Colors.black,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.7,
                child: ListView.builder(
                    itemCount: 10,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    // physics: NeverScrollableScrollPhysics(),
                    primary: true,
                    itemBuilder: (context, index) {
                      return Card(
                        child: Container(
                          height: 60,
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      height: 70,
                                      width: 70,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image:
                                                  AssetImage('assets/med1.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      // mainAxisAlignment:
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      // MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Femmoal Soap',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                        Text(
                                          'Rs 150',
                                          style: TextStyle(fontSize: 15),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(
                                  '${orderStatus[index]}',
                                  style: TextStyle(
                                      fontSize: 15, color: colorsList[index]),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
              // Container(
              //   child: TabBar(tabs: [
              //     Tab(
              //       text: 'All Orders',
              //     ),
              //     Tab(
              //       text: 'Pending',
              //     ),
              //     Tab(
              //       text: 'Complete',
              //     )
              //   ]),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
