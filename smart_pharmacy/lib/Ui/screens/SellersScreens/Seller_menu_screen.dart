// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:smart_pharmacy/components/2ndButton.dart';
// import 'package:smart_pharmacy/components/Custom_appbar.dart';
// import 'package:smart_pharmacy/constants/constant.dart';

// class MenuScreen extends StatefulWidget {
//   const MenuScreen({super.key});

//   @override
//   State<MenuScreen> createState() => _MenuScreenState();
// }

// class _MenuScreenState extends State<MenuScreen> {
//   Constants constants = Constants();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       // drawer:e,

//       endDrawer: Drawer(
//         clipBehavior: Clip.antiAlias,
//         width: MediaQuery.of(context).size.width * 0.65,
//         child: Padding(
//           padding: const EdgeInsets.all(12.0),
//           child: ListView(
//             children: [
//               Row(
//                 children: [
//                   Container(
//                     child: CircleAvatar(
//                       radius: 45,
//                       child: Text(
//                         'AR',
//                         style: GoogleFonts.abhayaLibre(fontSize: 30),
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   Container(
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       // mainAxisAlignment: MainAxisAlignment.start,
//                       children: [
//                         Text(
//                           'Ali Raza',
//                           style: GoogleFonts.abhayaLibre(
//                               fontSize: 25, color: Colors.white),
//                         ),
//                         Text(
//                           'User02',
//                           style: GoogleFonts.abhayaLibre(fontSize: 20),
//                         ),
//                       ],
//                     ),
//                   )
//                   // Column()
//                 ],
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               Column(
//                 // crossAxisAlignment: CrossAxisAlignment.center,
//                 children: [
//                   Container(
//                     child: Column(
//                       children: [
//                         SecondButton(
//                             title: 'Profile',
//                             radius: 0,
//                             borderColor: Colors.white,
//                             height: 40,
//                             textColor: Colors.white,
//                             fontSize: 20,
//                             onTap: () {},
//                             width: double.infinity),
//                         SecondButton(
//                             title: 'Seller Account',
//                             radius: 0,
//                             borderColor: Colors.white,
//                             height: 40,
//                             textColor: Colors.white,
//                             fontSize: 20,
//                             onTap: () {},
//                             width: double.infinity),
//                         SecondButton(
//                             title: 'Account Statement',
//                             radius: 0,
//                             borderColor: Colors.white,
//                             height: 40,
//                             textColor: Colors.white,
//                             fontSize: 20,
//                             onTap: () {},
//                             width: double.infinity),
//                         SecondButton(
//                             title: 'Payment',
//                             radius: 0,
//                             borderColor: Colors.white,
//                             height: 40,
//                             textColor: Colors.white,
//                             fontSize: 20,
//                             onTap: () {},
//                             width: double.infinity),
//                         SecondButton(
//                             title: 'Settings',
//                             radius: 0,
//                             borderColor: Colors.white,
//                             height: 40,
//                             textColor: Colors.white,
//                             fontSize: 20,
//                             onTap: () {},
//                             width: double.infinity),
//                         SecondButton(
//                             title: 'Help & Supports',
//                             radius: 0,
//                             borderColor: Colors.white,
//                             height: 40,
//                             textColor: Colors.white,
//                             fontSize: 20,
//                             onTap: () {},
//                             width: double.infinity),
//                         // SecondButton(
//                         //     title: 'Settings',
//                         //     radius: 0,
//                         //     borderColor: Colors.white,
//                         //     height: 40,
//                         //     textColor: Colors.white,
//                         //     fontSize: 20,
//                         //     onTap: () {},
//                         //     width: double.infinity),
//                       ],
//                     ),
//                   ),
//                   Container(
//                     margin: EdgeInsets.only(top: 160),
//                     // alignment:,
//                     child: SecondButton(
//                         title: 'Logout',
//                         radius: 0,
//                         borderColor: Colors.white,
//                         height: 40,
//                         textColor: Colors.white,
//                         fontSize: 20,
//                         onTap: () {},
//                         width: double.infinity),
//                   )
//                 ],
//               )
//               // DrawerHeader(
//               //     decoration: BoxDecoration(color: Colors.grey[350]),
//               //     child: Column(
//               //       children: [
//               //         Container(
//               //           child: Text('Name'),
//               //         ),
//               //         Container(
//               //           child: Text('Name'),
//               //         ),
//               //         Container(
//               //           child: Text('Name'),
//               //         ),
//               //         Container(
//               //           child: Text('Name'),
//               //         )
//               //       ],
//               //     ))
//             ],
//           ),
//         ),
//         backgroundColor: constants.buttonColor,
//       ),

//       // appBar: AppBar(
//       //   automaticallyImplyLeading: true,
//       //   title: Row(
//       //     children: [
//       //       Container(
//       //         height: 50,
//       //         width: 50,
//       //         decoration: BoxDecoration(
//       //             image: DecorationImage(
//       //                 image: AssetImage('assets/splashPicture.jpg'),
//       //                 fit: BoxFit.fill)),
//       //       ),
//       //       Text(
//       //         'Smart Pharmacy',
//       //         style: GoogleFonts.abhayaLibre(
//       //             fontSize: 25, fontWeight: FontWeight.normal),
//       //       )
//       //     ],
//       //   ),
//       // ),
//       body: Padding(
//         padding: const EdgeInsets.all(40.0),
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               CustomAppbar(),
//               SizedBox(
//                 height: 10,
//               ),
//               Center(
//                 child: Container(
//                   child: Text('Swipe left to show menu'),
//                 ),
//               ),
//               Center(
//                 child: Container(
//                   child: Icon(
//                     Icons.arrow_back,
//                     size: 60,
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
