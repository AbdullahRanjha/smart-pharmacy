import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SellerHelpSupportScreen extends StatefulWidget {
  const SellerHelpSupportScreen({super.key});

  @override
  State<SellerHelpSupportScreen> createState() =>
      _SellerHelpSupportScreenState();
}

class _SellerHelpSupportScreenState extends State<SellerHelpSupportScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Help & Support',
          style: GoogleFonts.abhayaLibre(fontSize: 25),
        ),
      ),
      body: Column(
        children: [],
      ),
    );
  }
}
