import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/constants/constant.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';

class RenameDashboardScreen extends StatefulWidget {
  const RenameDashboardScreen({super.key});

  @override
  State<RenameDashboardScreen> createState() => _RenameDashboardScreenState();
}

class _RenameDashboardScreenState extends State<RenameDashboardScreen> {
  List<String> orderStatus = ['27 Orders', '17 Pending', '9 Processing'];
  // final List<ChartData>
  // List<ChartData> data = [];
  final List<ChartData> chartData = [
    ChartData(2010, 35),
    ChartData(2011, 28),
    ChartData(2012, 34),
    ChartData(2013, 32),
    ChartData(2014, 40)
  ];
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/splashPicture.jpg'),
                        fit: BoxFit.fill)),
              ),
              Text(
                'Smart Pharmacy',
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
              )
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      shadowColor: Colors.grey,
                      elevation: 4,
                      child: Container(
                        height: 75,
                        width: 75,
                        decoration: BoxDecoration(
                            // color: Colors.teal,
                            // border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                                image: AssetImage('assets/Person.jpg'),
                                fit: BoxFit.cover)),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Abdullah Khan',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          'Al-Shifa Pharmacy',
                          style: TextStyle(
                              fontSize: 12, color: constants.buttonColor),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 85,
                  width: double.infinity,
                  child: ListView.builder(
                      itemCount: 3,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      primary: true,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: constants.buttonColor,
                                borderRadius: BorderRadius.circular(10)),
                            height: 80,
                            width: 110,
                            child: Center(
                                child: Text(
                              '${orderStatus[index]}',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.white),
                            )),
                          ),
                        );
                      }),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20))),
                      elevation: 4,
                      child: Container(
                        height: 30,
                        width: 180,
                        child: Center(
                          child: Text(
                            'Sales Statics',
                            style: TextStyle(fontSize: 14, color: Colors.black),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Card(
                        elevation: 4,
                        color: constants.buttonColor,
                        child: Container(
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: constants.buttonColor,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20),
                                  bottomRight: Radius.circular(20))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'By Month',
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),

                // Container()
                Container(
                    width: double.infinity,
                    height: 300,
                    child: SfCartesianChart(series: <ChartSeries>[
                      // Renders line chart
                      LineSeries<ChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (ChartData data, _) => data.x,
                          yValueMapper: (ChartData data, _) => data.y)
                    ])),

                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Top 10 Trending Products',
                    style: TextStyle(fontSize: 19),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),

                Card(
                  child: Container(
                    height: 140,
                    width: double.infinity,
                    child: ListView.builder(
                        itemCount: 3,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        primary: true,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              'https://scontent.fskt5-1.fna.fbcdn.net/v/t39.30808-6/376202403_3161993377439816_9075350442434924379_n.jpg?stp=cp0_dst-jpg&_nc_cat=105&ccb=1-7&_nc_sid=49d041&_nc_eui2=AeFn_fpWU1rMC0jHUA406WYXSH2W9r2ceLtIfZb2vZx4u2bgtHI8RvDum--4RmomoCl3Cx4gUucRwlXbj5bRddtg&_nc_ohc=1AqAm0iZPGUAX_viQUj&_nc_ht=scontent.fskt5-1.fna&oh=00_AfAXQiOCo6jlYymTZLw0fYLmFgDJQNhMBT2CnQ8hLAZXNw&oe=6503C6CE'),
                                          fit: BoxFit.fill),
                                      // color: Colors.grey,
                                      borderRadius: BorderRadius.circular(10)),
                                  height: 80,
                                  width: 110,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(
                                    10.0,
                                  ),
                                  child: Text(
                                    'Panadol Extra',
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: constants.buttonColor),
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                ),
                // Text('Panadol')
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ChartData {
  ChartData(this.x, this.y);
  final int x;
  final double y;
}
