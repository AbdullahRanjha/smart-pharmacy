import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SellerSettingPage extends StatefulWidget {
  const SellerSettingPage({super.key});

  @override
  State<SellerSettingPage> createState() => _SellerSettingPageState();
}

class _SellerSettingPageState extends State<SellerSettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Seller Settings',
          style: GoogleFonts.abhayaLibre(fontSize: 25),
        ),
      ),
      body: Column(
        children: [],
      ),
    );
  }
}
