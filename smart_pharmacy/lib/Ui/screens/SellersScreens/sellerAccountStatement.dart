import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class SellerAccountStatement extends StatefulWidget {
  const SellerAccountStatement({super.key});

  @override
  State<SellerAccountStatement> createState() => _SellerAccountStatementState();
}

class _SellerAccountStatementState extends State<SellerAccountStatement> {
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Account Statement',
            style: TextStyle(fontSize: 21),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                // SizedBox(
                //   height: 10,
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Today',
                      style: TextStyle(fontSize: 18),
                    ),
                    TextButton(onPressed: () {}, child: Text('See all'))
                  ],
                ),
                // SizedBox(
                //   height: 10,
                // ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Yesterday', style: TextStyle(fontSize: 17)),
                    TextButton(onPressed: () {}, child: Text('See all'))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails(),
                SizedBox(
                  height: 10,
                ),
                TransactionDetails()
              ],
            ),
          ),
        ));
  }

  Widget TransactionDetails() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: constants.whiteColor,
            boxShadow: [
              BoxShadow(
                offset: Offset(3, 3),
                blurRadius: 10,
                spreadRadius: 9,
                color: constants.buttonColor.withOpacity(0.2),
                // blurStyle: BlurStyle.normal,
              )
            ]),
        child: ListTile(
          trailing: Column(
            children: [
              Text(
                '100 USD',
                style: TextStyle(fontSize: 11),
              ),
              Text('Successful', style: TextStyle(fontSize: 11))
            ],
          ),
          leading: Container(
            height: double.infinity,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage(
                      'assets/med1.png',
                    ),
                    fit: BoxFit.fill)),
            width: 50,
          ),
          title: Text(
            'Med 1',
            style: TextStyle(fontSize: 18),
          ),
          subtitle: Container(
            // alignment: Alignment.topLeft,
            // color: Colors.red,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: Text('From: x12345abcd....',
                      style: TextStyle(fontSize: 11)),
                ),
                Text('June 26, 2023 06:23pm', style: TextStyle(fontSize: 11))
              ],
            ),
          ),
          // isThreeLine: true,
        ),
      ),
    );
  }
}
