import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/product_detailsScreen.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({super.key});

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  TextEditingController searchController = TextEditingController();

  Constants constants = Constants();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/splashPicture.jpg'),
                      fit: BoxFit.fill)),
            ),
            Text(
              'Smart Pharmacy',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(19.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              CustomtextFormField(
                  hintText: 'Search',
                  horizontalPadding: 0,
                  keyBoardtype: TextInputType.emailAddress,
                  lableText: 'Search',
                  controller: searchController,
                  radius: 10,
                  prefix: Icon(Icons.search),
                  // textFieldHeight: 15,
                  verticalPadding: 0),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  'My Products',
                  style: TextStyle(fontSize: 19),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                // color: Colors.red,
                height: MediaQuery.of(context).size.height * 0.3,
                child: ListView.builder(
                    itemCount: 3,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    // physics: NeverScrollableScrollPhysics(),
                    primary: true,
                    itemBuilder: (context, index) {
                      return Card(
                        child: Container(
                          height: 60,
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      height: 70,
                                      width: 70,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image:
                                                  AssetImage('assets/med1.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      // mainAxisAlignment:
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      // MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Femmoal Soap',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                        Text(
                                          'Rs 150',
                                          style: TextStyle(fontSize: 15),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Row(
                                  children: [
                                    Text(
                                      'Edit',
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    VerticalDivider(
                                      color: Colors.black,
                                    ),
                                    IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              Container(
                margin: EdgeInsets.only(top: 170),
                child: CustomButton(
                    backgroundColor: constants.buttonColor,
                    title: 'Add new Products',
                    radius: 10,
                    height: 45,
                    textColor: Colors.white,
                    fontSize: 20,
                    onTap: () {
                      Get.to(ProductDetailsScreen());
                    },
                    width: double.infinity),
              )
            ],
          ),
        ),
      ),
    );
  }
}
