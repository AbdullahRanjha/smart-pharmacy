import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buy_medicine_page.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/preception_screen.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class AdminDashboard extends StatefulWidget {
  const AdminDashboard({super.key});

  @override
  State<AdminDashboard> createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  Constants constants = Constants();

  List<String> images = [
    '/pharmacy1.jpg',
    '/pharmacy2.jpg',
    '/pharmacy3.jpg',
    '/pharmacy4.jpg',
    '/pharmacy1.jpg',
    '/pharmacy2.jpg',
    '/pharmacy3.jpg',
    '/pharmacy4.jpg',
    '/pharmacy1.jpg',
    '/pharmacy2.jpg',
  ];

  List<String> locations = [
    'Sarvaid Pharmacy\nG-13 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
    'Al-Slaeh Pharmacy\nG-14 Islamabad',
    'Sarvaid Pharmacy\nG-13 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
    'Al-Slaeh Pharmacy\nG-14 Islamabad',
    'Sarvaid Pharmacy\nG-13 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
    'Al-Slaeh Pharmacy\nG-14 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
  ];
  List<String> promotionsImages = ['/discount1.jpg', '/discount2.jpg'];
  List<String> promotionsText = ['Free Home Delivery', 'Get 20% discount'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10,
            top: 0,
          ),
          child: Column(
            children: [
              CustomAppbar(),
              Container(
                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(
                          'assets/11thPicture.jpg',
                        ))),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'What are you looking for?',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  )),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.18,
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BuyMedicineScreen()));
                      },
                      child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/12thPicture.jpg',
                                fit: BoxFit.fill,
                              ),
                              Center(
                                child: Text(
                                  'Buy Medicine',
                                  style: TextStyle(fontSize: 16),
                                ),
                              )
                            ],
                          )),
                    ),
                  ),
                  // SizedBox(
                  //   width: 15,
                  // ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.18,
                    width: MediaQuery.of(context).size.width * 0.45,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PreceptionScreen()));
                      },
                      child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset('assets/12thPicture.jpg',
                                  fit: BoxFit.fill),
                              Text(
                                'Upload Precptions',
                                style: TextStyle(fontSize: 16),
                              )
                            ],
                          )),
                    ),
                  )
                ],
              ),
              Container(
                  alignment: Alignment.topLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Stores near me.',
                        style: TextStyle(fontSize: 20),
                      ),
                      TextButton(onPressed: () {}, child: Text('Show All'))
                    ],
                  )),
              // SizedBox(
              //   height: 10,
              // ),
              Container(
                height: 185, // Adjust the height as needed
                child: ListView.builder(
                  shrinkWrap: true,
                  primary: true,

                  scrollDirection: Axis.horizontal, // Set to horizontal
                  itemCount: 10, // Replace with the actual item count
                  itemBuilder: (context, index) {
                    // Replace with your item widgets
                    return Container(
                      width: 150, // Adjust the width of each item
                      child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets${images[index]}',
                                fit: BoxFit.fill,
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Text(
                                  '${locations[index].toString()}',
                                  style: TextStyle(fontSize: 16),
                                ),
                              )
                            ],
                          )),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  'Promotions',
                  style: TextStyle(fontSize: 21),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 300,
                // Adjust the height as needed
                child: ListView.builder(
                  shrinkWrap: true,
                  primary: true,

                  scrollDirection: Axis.horizontal, // Set to horizontal
                  itemCount: 2, // Replace with the actual item count
                  itemBuilder: (context, index) {
                    // Replace with your item widgets
                    return Container(
                      width: 230, // Adjust the width of each item

                      child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets${promotionsImages[index]}',
                                fit: BoxFit.fill,
                              ),
                              Center(
                                child: Text(
                                  '${promotionsText[index].toString()}',
                                  style: TextStyle(fontSize: 17),
                                ),
                              )
                            ],
                          )),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
