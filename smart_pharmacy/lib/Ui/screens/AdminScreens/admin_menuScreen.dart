import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/admin_profile_screen.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/catagory_screen.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/offer_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_profile_screen.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/main_loginPage.dart';
import 'package:smart_pharmacy/components/2ndButton.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/constants/constant.dart';

import '../BuyerScreens/buyer_offer_screen1.dart';

class AdminMenuScreen extends StatefulWidget {
  const AdminMenuScreen({super.key});

  @override
  State<AdminMenuScreen> createState() => _AdminMenuScreenState();
}

class _AdminMenuScreenState extends State<AdminMenuScreen> {
  Constants constants = Constants();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // drawer:e,

        // endDrawer: Drawer(
        //   clipBehavior: Clip.antiAlias,
        //   width: MediaQuery.of(context).size.width * 0.65,
        //   child: Padding(
        //     padding: const EdgeInsets.all(12.0),
        //     child:
        //   ),
        //   backgroundColor: constants.buttonColor,
        // ),

        // appBar: AppBar(
        //   automaticallyImplyLeading: true,
        //   title: Row(
        //     children: [
        //       Container(
        //         height: 50,
        //         width: 50,
        //         decoration: BoxDecoration(
        //             image: DecorationImage(
        //                 image: AssetImage('assets/splashPicture.jpg'),
        //                 fit: BoxFit.fill)),
        //       ),
        //       Text(
        //         'Smart Pharmacy',
        //         style: GoogleFonts.abhayaLibre(
        //             fontSize: 25, fontWeight: FontWeight.normal),
        //       )
        //     ],
        //   ),
        // ),
        body: Row(
      children: [
        Container(
          height: double.infinity,
          color: Colors.grey[400],
          width: MediaQuery.of(context).size.width * 0.35,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.65,
          color: constants.buttonColor,
          height: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: [
                Row(
                  children: [
                    Container(
                      child: CircleAvatar(
                        radius: 45,
                        child: Text(
                          'AR',
                          style: TextStyle(fontSize: 27),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Shahzaib',
                            style: TextStyle(fontSize: 22, color: Colors.white),
                          ),
                          Text(
                            'Admin',
                            style: TextStyle(fontSize: 17),
                          ),
                        ],
                      ),
                    )
                    // Column()
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Column(
                        children: [
                          SecondButton(
                              title: 'Profile',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(AdminProfileScreen());
                              },
                              width: double.infinity),
                          SecondButton(
                              title: 'Catagories',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(CatagoryScreen());
                              },
                              width: double.infinity),

                          SecondButton(
                              title: 'Offers',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(BuyerOfferScreen());
                              },
                              width: double.infinity),
                          // SecondButton(
                          //     title: 'Security',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                          // SecondButton(
                          //     title: 'Settings',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                          // SecondButton(
                          //     title: 'Offers',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                          // SecondButton(
                          //     title: 'Help & Supports',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                          // SecondButton(
                          //     title: 'Settings',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 320),
                      // alignment:,
                      child: SecondButton(
                          title: 'Logout',
                          radius: 0,
                          borderColor: Colors.white,
                          height: 40,
                          textColor: Colors.white,
                          fontSize: 20,
                          onTap: () {
                            Get.to(MainLoginPage());
                          },
                          width: double.infinity),
                    )
                  ],
                )
                // DrawerHeader(
                //     decoration: BoxDecoration(color: Colors.grey[350]),
                //     child: Column(
                //       children: [
                //         Container(
                //           child: Text('Name'),
                //         ),
                //         Container(
                //           child: Text('Name'),
                //         ),
                //         Container(
                //           child: Text('Name'),
                //         ),
                //         Container(
                //           child: Text('Name'),
                //         )
                //       ],
                //     ))
              ],
            ),
          ),
        )
      ],
    ));
  }
}
