import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/admin_dashboardScreen.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/admin_menuScreen.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/catagory_screen.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class AdminMainScreen extends StatefulWidget {
  const AdminMainScreen({super.key});

  @override
  State<AdminMainScreen> createState() => _AdminMainScreenState();
}

class _AdminMainScreenState extends State<AdminMainScreen> {
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          backgroundColor: Colors.white,
          activeColor: Colors.blue,
          inactiveColor: Colors.black,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/home.jpg',
                fit: BoxFit.fill,
              ),
              label: 'Home',
              // label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Image.asset(
                'assets/Categorize.jpg',
                fit: BoxFit.fill,
              ),
              label: 'Orders',
            ),
            // BottomNavigationBarItem(
            //   icon: Badge(
            //     label: Text(
            //       '0',
            //       style: TextStyle(color: Colors.white),
            //     ),
            //     child: Icon(
            //       Icons.shopping_cart,
            //       color: constants.blackColor,
            //       size: 30,
            //     ),
            //   ),
            //   label: 'Cart',
            // ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.menu,
                size: 30,
              ),
              label: 'Menu',
            )

            // BottomNavigationBarItem(icon: )
          ],
        ),
        tabBuilder: (BuildContext context, index) {
          switch (index) {
            case 0:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(
                      child: SafeArea(child: AdminDashboard()));
                },
              );
            case 1:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: CatagoryScreen());
                },
              );
            case 2:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: AdminMenuScreen());
                },
              );
          }
          return Container();
        });
  }
}
