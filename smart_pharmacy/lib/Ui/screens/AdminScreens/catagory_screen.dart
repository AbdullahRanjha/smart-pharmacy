import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class CatagoryScreen extends StatefulWidget {
  const CatagoryScreen({super.key});

  @override
  State<CatagoryScreen> createState() => _CatagoryScreenState();
}

class _CatagoryScreenState extends State<CatagoryScreen> {
  TextEditingController searchController = TextEditingController();
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20),
        // padding: const EdgeInsets.only(
        //   left: 20.0,
        //   top: 30,
        // ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              CustomAppbar(),
              SizedBox(
                height: 10,
              ),
              CustomtextFormField(
                  prefix: Icon(Icons.search),
                  hintText: 'Search Catagory',
                  horizontalPadding: 0,
                  keyBoardtype: TextInputType.emailAddress,
                  lableText: 'Search Category',
                  controller: searchController,
                  radius: 10,
                  // textFieldHeight: 15,
                  verticalPadding: 0),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  'Catagories',
                  style: TextStyle(
                    fontSize: 19,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 70,
                // color: Colors,,
                // decoration: BoxDecoration(color: Colors.grey),
                child: Card(
                  color: constants.buttonColor,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          // decoration: BoxDecoration(color: Colors.),
                          child: Text(
                            'Medicine',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Text('Edit',
                                    style: TextStyle(color: Colors.white)),
                                VerticalDivider(
                                  color: Colors.black,
                                  thickness: 1,
                                ),
                                Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 70,
                // color: Colors,,
                // decoration: BoxDecoration(color: Colors.grey),
                child: Card(
                  color: constants.buttonColor,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          // decoration: BoxDecoration(color: Colors.),
                          child: Text(
                            'Face Products',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Text('Edit',
                                    style: TextStyle(color: Colors.white)),
                                VerticalDivider(
                                  color: Colors.black,
                                  thickness: 1,
                                ),
                                Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 70,
                // color: Colors,,
                // decoration: BoxDecoration(color: Colors.grey),
                child: Card(
                  color: constants.buttonColor,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          // decoration: BoxDecoration(color: Colors.),
                          child: Text(
                            'Vitamins',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Text('Edit',
                                    style: TextStyle(color: Colors.white)),
                                VerticalDivider(
                                  color: Colors.black,
                                  thickness: 1,
                                ),
                                Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 70,
                // color: Colors,,
                // decoration: BoxDecoration(color: Colors.grey),
                child: Card(
                  color: constants.buttonColor,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          // decoration: BoxDecoration(color: Colors.),
                          child: Text(
                            'Medicine',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Row(
                              children: [
                                Text('Edit',
                                    style: TextStyle(color: Colors.white)),
                                VerticalDivider(
                                  color: Colors.black,
                                  thickness: 1,
                                ),
                                Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 140),
                child: CustomButton(
                    backgroundColor: constants.buttonColor,
                    textColor: Colors.white,
                    title: 'Add New Catagory',
                    radius: 10,
                    height: 45,
                    fontSize: 20,
                    onTap: () {},
                    width: double.infinity),
              )
            ],
          ),
        ),
      ),
    );
  }
}
