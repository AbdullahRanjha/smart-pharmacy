import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class AdminOfferScreen extends StatefulWidget {
  const AdminOfferScreen({super.key});

  @override
  State<AdminOfferScreen> createState() => _AdminOfferScreenState();
}

class _AdminOfferScreenState extends State<AdminOfferScreen> {
  bool val1 = false;
  bool val2 = false;
  bool val3 = false;
  List<String> catagooriesNames = [
    'All Catagory',
    '2nd Catagory',
    '3rd Catagory'
  ];
  TextEditingController searchController = TextEditingController();
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Admin Offer Screen'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          children: [
            Container(
              color: constants.buttonColor,
              height: 150,
              width: double.infinity,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: CustomtextFormField(
                        hintText: 'Search',
                        horizontalPadding: 10,
                        keyBoardtype: TextInputType.emailAddress,
                        lableText: 'Search',
                        fillColor: Colors.white,
                        filled: true,
                        controller: searchController,
                        radius: 10,
                        verticalPadding: 10),
                  ),
                  // SizedBox(
                  //   height: 10,
                  // ),
                  Expanded(
                    child: Container(
                      // height: 70,
                      color: constants.buttonColor,
                      child: ListView.builder(
                        itemCount: catagooriesNames.length,
                        itemBuilder: (context, index) {
                          return Container(
                            // height: 20,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white),
                            child: Center(
                              child: Text(
                                  '${catagooriesNames[index].toString()}',
                                  style: GoogleFonts.abhayaLibre(fontSize: 14)),
                            ),
                          );
                        },
                        scrollDirection: Axis.horizontal,

                        // Row(
                        //   // crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: [
                        //     Padding(
                        //       padding: const EdgeInsets.only(left: 8.0),
                        //       child: Container(
                        //         height: 37,
                        //         decoration: BoxDecoration(
                        //             borderRadius: BorderRadius.circular(20),
                        //             color: Colors.white),
                        //         child: Center(
                        //           child: Padding(
                        //             padding: const EdgeInsets.all(8.0),
                        //             child: Text('All Catagory',
                        //                 style: GoogleFonts.abhayaLibre(
                        //                     fontSize: 16)),
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //     Padding(
                        //       padding: const EdgeInsets.only(left: 8.0),
                        //       child: Container(
                        //         height: 37,
                        //         decoration: BoxDecoration(
                        //             borderRadius: BorderRadius.circular(20),
                        //             color: Colors.white),
                        //         child: Center(
                        //           child: Padding(
                        //             padding: const EdgeInsets.all(8.0),
                        //             child: Text('All Catagory',
                        //                 style: GoogleFonts.abhayaLibre(
                        //                     fontSize: 16)),
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ],
                        // )
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: Container(
                // height: 100,
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 5),
                      blurRadius: 10,
                      spreadRadius: 9,
                      blurStyle: BlurStyle.normal,
                      color: constants.buttonColor.withOpacity(0.2))
                ]),
                child: ListView.builder(
                    itemCount: 10,
                    scrollDirection: Axis.vertical,
                    // physics: NeverScrollableScrollPhysics(),
                    primary: true,
                    itemBuilder: (context, index) {
                      return allCatagory();
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: 70,
                    width: 50,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                'https://pngimg.com/uploads/kfc/kfc_PNG33.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: constants.buttonColor),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text(
                                '15% Discount',
                                style: GoogleFonts.abhayaLibre(
                                    fontSize: 10, color: Colors.white),
                              ),
                            )),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Islamabad G-(8)Phase',
                                style: GoogleFonts.abhayaLibre(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: GoogleFonts.abhayaLibre(fontSize: 12),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Text(
                  'KFC',
                  style: GoogleFonts.abhayaLibre(fontSize: 25),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: GoogleFonts.abhayaLibre(color: constants.whiteColor),
              )),
              color: Colors.deepPurple,
            )
          ],
        ),
      ),
    );
  }
}
