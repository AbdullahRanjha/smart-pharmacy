import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Models/CartModel.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/main_page.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textbuttom.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class BuyMedicineScreen extends StatefulWidget {
  const BuyMedicineScreen({super.key});

  @override
  State<BuyMedicineScreen> createState() => _BuyMedicineScreenState();
}

class _BuyMedicineScreenState extends State<BuyMedicineScreen> {
  Constants constants = Constants();
  final List<String> medicineList = [
    '/med1.png',
    '/med2.png',
    '/med3.png',
    '/med1.png',
    '/med2.png',
    '/med3.png',
  ];
  List<String> medicineNames = [
    'Product Demo',
    'Product Demo',
    'Product Demo',
    'Product Demo',
    'Product Demo',
    'Product Demo'
  ];
  List<String> medicinePrices = ['150', '300', '567', '150', '300', '567'];
  // EndDrawer endDrawer = EndDrawer();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: Colors.red,
        appBar: AppBar(
            // automaticallyImplyLeading: false,
            title: Row(
          children: [
            Container(
              height: 70,
              width: 50,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/splashPicture.jpg'))),
            ),
            Text(
              'Smart Pharmacy',
              style: TextStyle(fontSize: 21, fontWeight: FontWeight.normal),
            ),
          ],
        )),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                // CustomAppbar(),
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/Doctor image.png'),
                          fit: BoxFit.fill)),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Shop by Catagory',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    MyTextButton(
                        color: constants.textButtonColor,
                        onTap: () {},
                        title: 'See All')
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Container(
                              height: 25,
                              width: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/lung 1.png'),
                                      fit: BoxFit.fill)),
                            ),
                            Text(
                              'Lungs Drugs',
                              style: TextStyle(fontSize: 13),
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Container(
                              height: 25,
                              width: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/kidni 1.png'),
                                      fit: BoxFit.fill)),
                            ),
                            Text(
                              'Kidney',
                              style: TextStyle(fontSize: 13),
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Container(
                              height: 25,
                              width: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/eye 1.png'),
                                      fit: BoxFit.fill)),
                            ),
                            Text(
                              'Eye Drugs',
                              style: TextStyle(fontSize: 13),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Recommend for you',
                    style: TextStyle(fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                GridView.builder(
                    itemCount: 6,
                    shrinkWrap: true,
                    // primary: true,
                    physics: NeverScrollableScrollPhysics(),
                    // scrollDirection: Axis.vertical,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 5,
                        crossAxisSpacing: 5),
                    itemBuilder: (context, index) {
                      return Card(
                        shadowColor: Colors.grey,
                        child: Container(
                          // color: Colors.red,
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: double.infinity,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.09,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets${medicineList[index]}'),
                                        fit: BoxFit.fill)),
                              ),
                              Text(
                                '${medicineNames[index].toString()}\n' +
                                    "Rs: ${medicinePrices[index].toString()}",
                                style: TextStyle(fontSize: 13),
                              ),
                              CustomButton(
                                  title: 'Add to cart',
                                  radius: 10,
                                  // textColor:,
                                  backgroundColor: constants.buttonColor,
                                  height: 30,
                                  fontSize: 15,
                                  onTap: () {
                                    Fluttertoast.showToast(
                                        gravity: ToastGravity.BOTTOM,
                                        backgroundColor: Colors.black,
                                        textColor: Colors.white,
                                        msg: 'Item added in Cart');
                                    CartItems.cartItems.add(CartItems(
                                        name: Products.productsList[index].name,
                                        price: Products
                                            .productsList[index].price
                                            .toInt(),
                                        totalPrice:
                                            Products.productsList[index].price,
                                        Quantity: 1,
                                        productId: Products
                                            .productsList[index].productId,
                                        url: Products.productsList[index].url));

                                    // setState(() {
                                    //   // CartItems.totalQuantity(index);
                                    // });
                                    // CartItems(
                                    //     quantity: Products
                                    //         .productsList[index].productId);
                                  },
                                  textColor: constants.whiteColor,
                                  width: double.infinity)
                            ],
                          ),
                        ),
                      );
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
