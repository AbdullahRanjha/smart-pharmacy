import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class PaymentTrackerScreen extends StatefulWidget {
  const PaymentTrackerScreen({super.key});

  @override
  State<PaymentTrackerScreen> createState() => _PaymentTrackerScreenState();
}

class _PaymentTrackerScreenState extends State<PaymentTrackerScreen> {
  Constants constants = Constants();
  TextEditingController treckingIdController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            // shadowColor: Colors.grey[200],
            automaticallyImplyLeading: true,
            // elevation: 0.4,
            title: Row(
              children: [
                Container(
                  height: 70,
                  width: 50,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/splashPicture.jpg'))),
                ),
                Text(
                  'Smart Pharmacy',
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.normal),
                ),
              ],
            )),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: constants.buttonColor,
                        borderRadius: BorderRadius.circular(10)),
                    // alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Enter your Tracking Number',
                              style:
                                  TextStyle(fontSize: 20, color: Colors.white),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                // width: Get.width * 0.,
                                height: 80,
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10,
                                        // top: 10,
                                        bottom: 10,
                                        right: 0),
                                    child: CustomtextFormField(
                                      filled: true,
                                      fillColor: Colors.white,
                                      // labelColor: Colors.blue,s
                                      hintText: 'Tracking Id',
                                      focusColor: Colors.white,
                                      horizontalPadding: 0,
                                      keyBoardtype: TextInputType.emailAddress,
                                      lableText: 'Tracking Id',
                                      radius: 10,
                                      verticalPadding: 0,
                                      controller: treckingIdController,
                                    )),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 18, right: 10, bottom: 10, left: 0),
                              child: InkWell(
                                onTap: () {
                                  print('object');
                                },
                                child: Container(
                                  height: 47,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[350],
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10.0),
                                          bottomRight: Radius.circular(10.0))),
                                  child: Center(
                                    child: Text(
                                      'Track',
                                      style:
                                          GoogleFonts.abhayaLibre(fontSize: 18),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                // TextFormField(
                //   controller: treckingIdController,
                //   decoration:
                //       InputDecoration(filled: true, fillColor: Colors.blue),
                // )
                SizedBox(
                  height: 20,
                ),
                Card(
                  // color: Colors.gre,
                  shadowColor: Colors.grey,
                  elevation: 4,
                  child: Container(
                    height: 150,
                    width: double.infinity,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Column(
                                  children: [
                                    Text('#467856475629'),
                                    Container(
                                        // alignment: Alignment.t,
                                        child: Text('Femmoal'))
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: CustomButton(
                                    title: 'Sent',
                                    backgroundColor: constants.buttonColor,
                                    radius: 10,
                                    height: 40,
                                    fontSize: 20,
                                    textColor: Colors.white,
                                    onTap: () {},
                                    width: 100),
                              ),
                            )
                          ],
                        ),
                        Divider(
                          color: Colors.black,
                          thickness: 1.5,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  child: Column(
                                children: [
                                  Text('Servaid Pharmacy'),
                                  Text('G-13 Islamabad')
                                ],
                              )),
                              Icon(Icons.arrow_forward),
                              Container(
                                  child: Column(
                                children: [
                                  Text('Ali Raza'),
                                  Text('G-14 Islamabad')
                                ],
                              )),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
