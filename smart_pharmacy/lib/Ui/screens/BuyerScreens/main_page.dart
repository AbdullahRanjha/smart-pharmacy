import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/cart_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/dashboard_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/drawerScreen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/preception_screen.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class BuyerHomePage extends StatefulWidget {
  const BuyerHomePage({super.key});

  @override
  State<BuyerHomePage> createState() => _BuyerHomePageState();
}

class _BuyerHomePageState extends State<BuyerHomePage> {
  Constants constants = Constants();

  List<String> images = [
    '/pharmacy1.jpg',
    '/pharmacy2.jpg',
    '/pharmacy3.jpg',
    '/pharmacy4.jpg',
    '/pharmacy1.jpg',
    '/pharmacy2.jpg',
    '/pharmacy3.jpg',
    '/pharmacy4.jpg',
    '/pharmacy1.jpg',
    '/pharmacy2.jpg',
  ];

  List<String> locations = [
    'Sarvaid Pharmacy\nG-13 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
    'Al-Slaeh Pharmacy\nG-14 Islamabad',
    'Sarvaid Pharmacy\nG-13 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
    'Al-Slaeh Pharmacy\nG-14 Islamabad',
    'Sarvaid Pharmacy\nG-13 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
    'Al-Slaeh Pharmacy\nG-14 Islamabad',
    'D.Watson Pharmacy\nG-13 Islamabad',
  ];
  List<String> promotionsImages = ['/discount1.jpg', '/discount2.jpg'];
  List<String> promotionsText = ['Free Home Delivery', 'Get 20% discount'];

  int selectedIndex = 0;

  void ontappedIndex(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  // final List pages = [
  //   DashboardScreen(),
  //   PreceptionScreen(),
  //   CartScreen(),
  //   EndDrawer()
  //   // BuyMedicineScreen()/
  // ];

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          activeColor: Colors.blue,
          inactiveColor: Colors.black,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                size: 30,
                color: constants.blackColor,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.upload_file,
                color: constants.blackColor,
                size: 30,
              ),
              label: 'Preceptions',
            ),
            BottomNavigationBarItem(
              icon: Badge(
                label: Text(
                  '0',
                  style: TextStyle(color: Colors.white),
                ),
                child: Icon(
                  Icons.shopping_cart,
                  color: constants.blackColor,
                  size: 30,
                ),
              ),
              label: 'Cart',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.menu,
                size: 30,
              ),
              label: 'Menu',
            )

            // BottomNavigationBarItem(icon: )
          ],
        ),
        tabBuilder: (BuildContext context, index) {
          switch (index) {
            case 0:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(
                      child: SafeArea(child: DashboardScreen()));
                },
              );
            case 1:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: PreceptionScreen());
                },
              );
            case 2:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: CartScreen());
                },
              );
            case 3:
              return CupertinoTabView(
                builder: (context) {
                  return CupertinoPageScaffold(child: EndDrawer());
                },
              );
            // break;
            // default:
          }
          return Container();
        });
    // return SafeArea(
    //     child: Cupert(
    //         bottomNavigationBar: BottomNavigationBar(
    //             // BottomNavigationBarType()
    //             // type: Botto,
    //             currentIndex: selectedIndex,
    //             unselectedItemColor: Colors.black,
    //             selectedItemColor: Colors.blue,
    //             onTap: ontappedIndex,
    //             items: <BottomNavigationBarItem>[
    //               BottomNavigationBarItem(
    //                 icon: Icon(
    //                   Icons.home,
    //                   size: 30,
    //                   color: constants.blackColor,
    //                 ),
    //                 label: 'Home',
    //               ),
    //               BottomNavigationBarItem(
    //                 icon: Icon(
    //                   Icons.upload_file,
    //                   color: constants.blackColor,
    //                   size: 30,
    //                 ),
    //                 label: 'Preceptions',
    //               ),
    //               BottomNavigationBarItem(
    //                 icon: Badge(
    //                   label: Text(
    //                     '0',
    //                     style: TextStyle(color: Colors.white),
    //                   ),
    //                   child: Icon(
    //                     Icons.shopping_cart,
    //                     color: constants.blackColor,
    //                     size: 30,
    //                   ),
    //                 ),
    //                 label: 'Cart',
    //               ),
    //               BottomNavigationBarItem(
    //                 icon: Icon(
    //                   Icons.menu,
    //                   size: 30,
    //                 ),
    //                 label: 'Menu',
    //               )
    //               // BottomNavigationBarItem(
    //               //     icon: Icon(
    //               //       Icons.menu,
    //               //       size: 30,
    //               //       color: constants.blackColor,
    //               //     ),
    //               //     label: 'Menu'),
    //             ]),
    //         appBar: AppBar(
    //             // shadowColor: Colors.grey[200],
    //             automaticallyImplyLeading: false,
    //             // elevation: 0.4,
    //             title: Row(
    //               children: [
    //                 Container(
    //                   height: 70,
    //                   width: 50,
    //                   decoration: BoxDecoration(
    //                       image: DecorationImage(
    //                           image: AssetImage('assets/splashPicture.jpg'))),
    //                 ),
    //                 Text(
    //                   'Smart Pharmacy',
    //                   style: GoogleFonts.abhayaLibre(
    //                       fontSize: 25, fontWeight: FontWeight.normal),
    //                 ),
    //               ],
    //             )),
    //         body: pages[selectedIndex])
    //         );
  }
}


