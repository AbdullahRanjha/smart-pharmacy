import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class BuyerOfferScreen extends StatefulWidget {
  const BuyerOfferScreen({super.key});

  @override
  State<BuyerOfferScreen> createState() => _BuyerOfferScreenState();
}

class _BuyerOfferScreenState extends State<BuyerOfferScreen> {
  bool val1 = true;
  bool val2 = false;
  bool val3 = false;
  bool val4 = false;
  bool val5 = false;
  bool val6 = false;

  List<String> catagooriesNames = [
    'All Catagory',
    '2nd Catagory',
    '3rd Catagory',
    '4th Catagory',
    '5th Catagory',
    '6th Catagory'
  ];
  TextEditingController searchController = TextEditingController();
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Offer Screen'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: constants.buttonColor,
                  borderRadius: BorderRadius.circular(15)),
              height: 150,
              width: double.infinity,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: CustomtextFormField(
                        hintText: 'Search',
                        horizontalPadding: 10,
                        keyBoardtype: TextInputType.emailAddress,
                        lableText: 'Search',
                        fillColor: Colors.white,
                        filled: true,
                        controller: searchController,
                        radius: 10,
                        verticalPadding: 10),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Container(
                        // height: 70,
                        color: constants.buttonColor,
                        child: ListView.builder(
                          shrinkWrap: true,
                          primary: true,
                          scrollDirection: Axis.horizontal,
                          physics: AlwaysScrollableScrollPhysics(),
                          itemCount: catagooriesNames.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                if (index == 0) {
                                  val1 = true;
                                  val2 = false;
                                  val3 = false;
                                  val4 = false;
                                  val5 = false;
                                  val6 = false;
                                  print(val1.toString());
                                  // print(val1);
                                  print(val2.toString());
                                  print(val3.toString());
                                  // setState(() {});
                                } else if (index == 1) {
                                  val1 = false;
                                  val2 = true;
                                  val4 = false;
                                  val5 = false;
                                  val6 = false;
                                  val3 = false;
                                  print(val1.toString());
                                  print(val2.toString());
                                  print(val3.toString());
                                  // setState(() {});
                                } else if (index == 2) {
                                  val1 = false;
                                  val2 = false;
                                  val3 = true;
                                  val4 = false;
                                  val5 = false;
                                  val6 = false;
                                  print(val1.toString());
                                  print(val2.toString());
                                  print(val3.toString());
                                  // setState(() {});
                                } else if (index == 3) {
                                  val1 = false;
                                  val2 = false;
                                  val3 = false;
                                  val4 = true;
                                  val5 = false;
                                  val6 = false;
                                } else if (index == 4) {
                                  val1 = false;
                                  val2 = false;
                                  val3 = false;
                                  val4 = false;
                                  val5 = true;
                                  val6 = false;
                                } else if (index == 5) {
                                  val1 = false;
                                  val2 = false;
                                  val3 = false;
                                  val4 = false;
                                  val5 = false;
                                  val6 = true;
                                }
                                setState(() {});
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: Container(
                                  height: 20,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: Colors.white),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                          '${catagooriesNames[index].toString()}',
                                          style: GoogleFonts.abhayaLibre(
                                              fontSize: 14)),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },

                          // Row(
                          //   // crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: [
                          //     Padding(
                          //       padding: const EdgeInsets.only(left: 8.0),
                          //       child: Container(
                          //         height: 37,
                          //         decoration: BoxDecoration(
                          //             borderRadius: BorderRadius.circular(20),
                          //             color: Colors.white),
                          //         child: Center(
                          //           child: Padding(
                          //             padding: const EdgeInsets.all(8.0),
                          //             child: Text('All Catagory',
                          //                 style: GoogleFonts.abhayaLibre(
                          //                     fontSize: 16)),
                          //           ),
                          //         ),
                          //       ),
                          //     ),
                          //     Padding(
                          //       padding: const EdgeInsets.only(left: 8.0),
                          //       child: Container(
                          //         height: 37,
                          //         decoration: BoxDecoration(
                          //             borderRadius: BorderRadius.circular(20),
                          //             color: Colors.white),
                          //         child: Center(
                          //           child: Padding(
                          //             padding: const EdgeInsets.all(8.0),
                          //             child: Text('All Catagory',
                          //                 style: GoogleFonts.abhayaLibre(
                          //                     fontSize: 16)),
                          //           ),
                          //         ),
                          //       ),
                          //     ),
                          //   ],
                          // )
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 13),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: Container(
                // height: 100,
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 5),
                      blurRadius: 10,
                      spreadRadius: 9,
                      blurStyle: BlurStyle.normal,
                      color: constants.buttonColor.withOpacity(0.2))
                ]),
                child: ListView.builder(
                    itemCount: 10,
                    scrollDirection: Axis.vertical,
                    // physics: NeverScrollableScrollPhysics(),
                    primary: true,
                    itemBuilder: (context, index) {
                      // return allCatagory();
                      if (val1 == true)
                        return allCatagory1();
                      else if (val2 == true)
                        return allCatagory2();
                      else if (val3 == true)
                        return allCatagory3();
                      else if (val4 == true)
                        return allCatagory4();
                      else if (val5 == true)
                        return allCatagory5();
                      else if (val6 == true) return allCatagory6();
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory1() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: double.infinity,
                    width: 40,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/med1.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: constants.buttonColor),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  '15% Discount',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Multan G-(8)',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    'Catagory 1',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: TextStyle(color: constants.whiteColor, fontSize: 15),
              )),
              color: constants.buttonColor,
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory2() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: double.infinity,
                    width: 40,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/med2.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: constants.buttonColor),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  '15% Discount',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Multan G-(8)',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    'Catagory 2',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: TextStyle(color: constants.whiteColor, fontSize: 15),
              )),
              color: constants.buttonColor,
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory3() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: double.infinity,
                    width: 40,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/med3.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: constants.buttonColor),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  '15% Discount',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Multan G-(8)',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    'Catagory 3',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: TextStyle(color: constants.whiteColor, fontSize: 15),
              )),
              color: constants.buttonColor,
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory4() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: double.infinity,
                    width: 40,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/eye 1.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: constants.buttonColor),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  '15% Discount',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Multan G-(8)',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    'Catagory 4',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: TextStyle(color: constants.whiteColor, fontSize: 15),
              )),
              color: constants.buttonColor,
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory5() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: double.infinity,
                    width: 40,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/kidni 1.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: constants.buttonColor),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  '15% Discount',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Multan G-(8)',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    'Catagory 5',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: TextStyle(color: constants.whiteColor, fontSize: 15),
              )),
              color: constants.buttonColor,
            )
          ],
        ),
      ),
    );
  }

  Widget allCatagory6() {
    return Container(
      // color: Colors.blue,
      child: Card(
        // shadowColor: constants.blackColor,
        // elevation: 7,
        // color: constants.buttonColor,
        color: constants.whiteColor,
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                leading: Container(
                    // alignment: Alignment.centerLeft,
                    height: double.infinity,
                    width: 40,
                    decoration: BoxDecoration(
                        // color: Colors.red,
                        // border: Border.all(color: constants.),
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/lung 1.png')))),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 3.0),
                          child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: constants.buttonColor),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  '15% Discount',
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          child: Row(
                            children: [
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                CupertinoIcons.location,
                                size: 10,
                              ),
                              Text(
                                'Multan G-(8)',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      child: Text(
                        '15% Discount on this Medicine',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                // trailing: Container(
                //   height: double.infinity,
                //   width: 10,
                //   color: constants.buttonColor,
                // ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 3.0),
                  child: Text(
                    'Catagory 6',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ),
            Container(
              width: 40,
              height: 100,
              child: Center(
                  child: Text(
                '15%',
                style: TextStyle(color: constants.whiteColor, fontSize: 15),
              )),
              color: constants.buttonColor,
            )
          ],
        ),
      ),
    );
  }
}
