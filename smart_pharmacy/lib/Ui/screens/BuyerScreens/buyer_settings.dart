import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BuyerSettingsScreen extends StatefulWidget {
  const BuyerSettingsScreen({super.key});

  @override
  State<BuyerSettingsScreen> createState() => _BuyerSettingsScreenState();
}

class _BuyerSettingsScreenState extends State<BuyerSettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Buyer Settings',
          style: GoogleFonts.abhayaLibre(fontSize: 25),
        ),
      ),
      body: Column(
        children: [],
      ),
    );
  }
}
