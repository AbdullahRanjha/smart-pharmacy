import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
// import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Models/CartModel.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/cart_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/payment_tracker_page.dart';
// import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/constants/constant.dart';
import 'package:smart_pharmacy/controllers/radioButton_controller.dart';

class CheckOutScreen extends StatefulWidget {
  const CheckOutScreen({super.key});

  @override
  State<CheckOutScreen> createState() => _CheckOutScreenState();
}

class _CheckOutScreenState extends State<CheckOutScreen> {
  RadioController controller = RadioController();

  CartScreen cartScreen = CartScreen();
  Constants constants = Constants();
  var value1;
  // var count;
  // var value2;

  void addQuantity(int index) {
    // var value;
    value1 = CartItems.cartItems[index].Quantity++;
    // print(CartItems.cartItems[index].Quantity);
    print('Total Quantity is' + value1.toString());
  }

  decreaseQuantity(int index) {
    // var value;
    value1 = CartItems.cartItems[index].Quantity--;
    print('Total Quantity is' + value1.toString());
  }

  removeContainer(int index) {
    CartItems.cartItems.removeAt(index);
  }

  var totalPrice;
  totalPrice1(int index) {
    totalPrice =
        CartItems.cartItems[index].Quantity * CartItems.cartItems[index].price;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            // shadowColor: Colors.grey[200],
            automaticallyImplyLeading: true,
            // elevation: 0.4,
            title: Row(
              children: [
                Container(
                  height: 70,
                  width: 50,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/splashPicture.jpg'))),
                ),
                Text(
                  'Smart Pharmacy',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.normal),
                ),
              ],
            )),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                // CustomAppbar(),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: constants.buttonColor,
                        borderRadius: BorderRadius.circular(10)),
                    // alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Center(
                        child: Text(
                      'Check Out',
                      style: TextStyle(color: Colors.white, fontSize: 22),
                    )),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.3,
                  child: ListView.builder(
                      itemCount: CartItems.cartItems.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) {
                        return Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: double.infinity,
                          child: Card(
                            shadowColor: Colors.grey,
                            elevation: 3,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: double.infinity,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              '${CartItems.cartItems[index].url}'),
                                          fit: BoxFit.fill)),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      '${CartItems.cartItems[index].name}',
                                    ),
                                    Text(
                                        '${CartItems.cartItems[index].price.toString()}')
                                  ],
                                ),
                                Container(
                                  // color: Colors.grey,
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          addQuantity(index);
                                          setState(() {});
                                        },
                                        child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.grey,
                                                borderRadius:
                                                    BorderRadius.circular(30)),
                                            // height: 25,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.035,
                                            alignment: Alignment.center,
                                            // color: Colors.grey,

                                            child: Icon(
                                              Icons.add,
                                              color: Colors.white,
                                            )),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.035,
                                        // height: 30,
                                        width: 25,
                                        child: Center(
                                            child: Text(
                                          '${CartItems.cartItems[index].Quantity.toString()}',
                                          style: TextStyle(fontSize: 17),
                                        )),
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          decreaseQuantity(index);
                                          setState(() {});
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.grey,
                                              borderRadius:
                                                  BorderRadius.circular(30)),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.035,
                                          child: Icon(
                                            Icons.remove,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: IconButton(
                                      onPressed: () {
                                        Fluttertoast.showToast(
                                            gravity: ToastGravity.BOTTOM,
                                            backgroundColor: Colors.black,
                                            textColor: Colors.white,
                                            msg: 'Item removed');
                                        removeContainer(index);
                                        setState(() {});
                                      },
                                      icon: Icon(
                                        Icons.delete,
                                        size: 30,
                                        color: Colors.red,
                                      )),
                                ),
                              ],
                            ),
                          ),
                        );
                        //     Container(
                        //   child: Text(CartItems.cartItems[index].name),
                        // );
                      }),
                ),

                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Card(
                    shadowColor: Colors.grey,
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Container(
                      decoration: BoxDecoration(
                          color: constants.buttonColor,
                          borderRadius: BorderRadius.circular(20)),
                      // alignment: Alignment.center,
                      height: MediaQuery.of(context).size.height * 0.06,
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Center(
                          child: Text(
                        'Payment Method',
                        style: TextStyle(color: Colors.white, fontSize: 21),
                      )),
                    ),
                  ),
                ),
                // SizedBox(
                //   height: 10,
                // ),
                Card(
                  shadowColor: Colors.grey,
                  elevation: 4,
                  child: Container(
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.grey)),
                    child: Column(
                      children: [
                        Obx(() => RadioListTile(
                            contentPadding: EdgeInsets.all(0),
                            value: "1",
                            groupValue: controller.selectedValue.value,
                            title: Text('Cash on delivery (COD)'),
                            onChanged: (val) {
                              controller.changeSelectedValue(val!);
                            })),
                        Obx(() => RadioListTile(
                            contentPadding: EdgeInsets.all(0),
                            value: "2",
                            title: Text('By Card (Visa/Master)'),
                            groupValue: controller.selectedValue.value,
                            onChanged: (val) {
                              controller.changeSelectedValue(val!);
                            })),
                        Obx(() => RadioListTile(
                            value: "3",
                            title: Text('By Easypaisa/JazzCash'),
                            contentPadding: EdgeInsets.all(0),
                            groupValue: controller.selectedValue.value,
                            onChanged: (val) {
                              controller.changeSelectedValue(val!);
                            })),
                        CustomButton(
                            title: 'Select',
                            backgroundColor: constants.buttonColor,
                            textColor: Colors.white,
                            radius: 10,
                            height: MediaQuery.of(context).size.height * 0.04,
                            fontSize: 21,
                            onTap: () {
                              Fluttertoast.showToast(
                                  msg: 'Payment method selected',
                                  gravity: ToastGravity.BOTTOM,
                                  backgroundColor: Colors.black);
                              Get.to(PaymentTrackerScreen());
                              // Navigator.push(context, route)
                            },
                            width: MediaQuery.of(context).size.width * 0.4),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                )
                // Center(child: Card(child: Container(),),)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
