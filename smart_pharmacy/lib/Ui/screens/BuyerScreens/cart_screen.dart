import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Models/CartModel.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/check_out_screen.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/components/textbuttom.dart';
import 'package:smart_pharmacy/constants/constant.dart';
import 'package:smart_pharmacy/controllers/radioButton_controller.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  final TextEditingController pharmasistController = TextEditingController();
  RadioController controller = Get.put(RadioController());
  int selectedOption = 1;

  CartScreen cartScreen = CartScreen();
  var value1;
  // var count;
  // var value2;

  void addQuantity(int index) {
    // var value;
    value1 = CartItems.cartItems[index].Quantity++;
    // print(CartItems.cartItems[index].Quantity);
    print('Total Quantity is' + value1.toString());
  }

  decreaseQuantity(int index) {
    // var value;
    value1 = CartItems.cartItems[index].Quantity--;
    print('Total Quantity is' + value1.toString());
  }

  removeContainer(int index) {
    CartItems.cartItems.removeAt(index);
  }

  var totalPrice;
  totalPrice1(int index) {
    totalPrice =
        CartItems.cartItems[index].Quantity * CartItems.cartItems[index].price;
    setState(() {});
  }

  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 0.0, left: 10, right: 10),
            child: Column(
              children: [
                CustomAppbar(),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: constants.buttonColor,
                        borderRadius: BorderRadius.circular(10)),
                    // alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Center(
                        child: Text(
                      'My Cart',
                      style: TextStyle(color: Colors.white, fontSize: 22),
                    )),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.3,
                  child: ListView.builder(
                      itemCount: CartItems.cartItems.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) {
                        return Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: double.infinity,
                          child: Card(
                            shadowColor: Colors.grey,
                            elevation: 3,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CircleAvatar(
                                  radius: 30,
                                  backgroundImage: NetworkImage(
                                      CartItems.cartItems[index].url),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      CartItems.cartItems[index].name,
                                    ),
                                    Text(
                                        'Rs ${CartItems.cartItems[index].price}/-')
                                  ],
                                ),
                                Container(
                                  // color: Colors.grey,
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.grey,
                                                borderRadius:
                                                    BorderRadius.circular(30)),
                                            // height: 25,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.035,
                                            alignment: Alignment.center,
                                            // color: Colors.grey,
                                            child: InkWell(
                                              onTap: () {
                                                addQuantity(index);
                                                setState(() {});
                                              },
                                              child: Icon(
                                                Icons.add,
                                                color: Colors.white,
                                              ),
                                            )),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.035,
                                        // height: 30,
                                        width: 30,
                                        child: Center(
                                            child: Text(
                                          '${CartItems.cartItems[index].Quantity}',
                                          style: GoogleFonts.abhayaLibre(
                                              fontSize: 20),
                                        )),
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          decreaseQuantity(index);
                                          setState(() {});
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.grey,
                                              borderRadius:
                                                  BorderRadius.circular(30)),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.035,
                                          child: Icon(
                                            Icons.remove,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  child: IconButton(
                                      onPressed: () {
                                        Fluttertoast.showToast(
                                            gravity: ToastGravity.BOTTOM,
                                            backgroundColor: Colors.black,
                                            textColor: Colors.white,
                                            msg: 'Item removed');
                                        removeContainer(index);
                                        setState(() {});
                                      },
                                      icon: Icon(
                                        Icons.delete,
                                        size: 30,
                                        color: Colors.red,
                                      )),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Note for the Pharmasist',
                      style: TextStyle(fontSize: 19),
                    )),
                Container(
                    // height: MediaQuery.of(context).size.height * 0.,
                    child: CustomtextFormField(
                        hintText: 'Note for the Pharmasist',
                        horizontalPadding: 0,
                        // textFieldHeight: 50,
                        keyBoardtype: TextInputType.emailAddress,
                        lableText: 'Note',
                        controller: pharmasistController,
                        radius: 10,
                        verticalPadding: 0)),
                SizedBox(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.only(top: 90),
                  // alignment: Alignment.bottomCenter
                  decoration: BoxDecoration(
                      color: Colors.grey[350],
                      borderRadius: BorderRadius.circular(10)),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 20),
                          child: Text(
                            '10 items | Rs 260/-',
                            style: TextStyle(fontSize: 15),
                          )),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Container(
                          child: CustomButton(
                              title: 'Add to Cart',
                              backgroundColor: constants.buttonColor,
                              radius: 10,
                              textColor: Colors.white,
                              height: MediaQuery.of(context).size.height * 0.05,
                              fontSize: 17,
                              onTap: () {
                                Get.to(CheckOutScreen());

                                int? index;
                                double? index2;
                                CartItems(
                                    name: Products.productsList[index!].name,
                                    price: Products
                                        .productsList[index2!.toInt()].price
                                        .toInt(),
                                    Quantity: 1,
                                    productId:
                                        Products.productsList[index].productId,
                                    url: Products.productsList[index].url);
                              },
                              width: MediaQuery.of(context).size.width * 0.35),
                        ),
                      )
                    ],
                  ),
                ),
                // RadioListTile<ProductTypeEnum>(
                //     value: ProductTypeEnum.By_Card,
                //     groupValue: productTypeEnum,
                //     title: Text('Cash on Delivery (COD)'),
                //     onChanged: (val) {
                //       productTypeEnum = val;
                //       setState(() {});
                //     }),
                // RadioListTile<ProductTypeEnum>(
                //     value: ProductTypeEnum.Cash_on_Delivery,
                //     title: Text('Card)'),
                //     groupValue: productTypeEnum,
                //     onChanged: (val) {
                //       productTypeEnum = val;
                //       setState(() {});
                //     }),
                // RadioListTile<ProductTypeEnum>(
                //     value: ProductTypeEnum.By_JazzCash_or_EasyPaisa,
                //     groupValue: productTypeEnum,
                //     title: Text('JazzCash/EasyPaisa'),
                //     onChanged: (val) {
                //       productTypeEnum = val;
                //       setState(() {});
                //     })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
