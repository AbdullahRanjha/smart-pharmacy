import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class BuyerOrderScreen extends StatefulWidget {
  const BuyerOrderScreen({super.key});

  @override
  State<BuyerOrderScreen> createState() => _BuyerOrderScreenState();
}

class _BuyerOrderScreenState extends State<BuyerOrderScreen> {
  Constants constants = Constants();
  List<String> orderStatus = [
    'Successful',
    'Pending',
    'Cancel',
    'Successful',
    'Pending',
    'Cancel',
    'Successful',
    'Pending',
    'Cancel',
    'Successful',
    'Pending',
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Buyer Order Screen',
              style: GoogleFonts.abhayaLibre(fontSize: 25),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Today',
                          style: GoogleFonts.abhayaLibre(fontSize: 20),
                        ),
                        TextButton(onPressed: () {}, child: Text('See all'))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Yesterday',
                            style: GoogleFonts.abhayaLibre(fontSize: 20)),
                        TextButton(onPressed: () {}, child: Text('See all'))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails(),
                  SizedBox(
                    height: 10,
                  ),
                  TransactionDetails()
                ],
              ),
            ),
          )),
    );
  }

  Widget TransactionDetails() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: constants.whiteColor,
            boxShadow: [
              BoxShadow(
                offset: Offset(3, 3),
                blurRadius: 10,
                spreadRadius: 9,
                color: constants.buttonColor.withOpacity(0.2),
                // blurStyle: BlurStyle.normal,
              )
            ]),
        child: ListTile(
          trailing: Column(
            children: [
              Text(
                '100 USDT',
                style: GoogleFonts.abhayaLibre(fontSize: 13),
              ),
              Text('Successful', style: GoogleFonts.abhayaLibre(fontSize: 13))
            ],
          ),
          leading: Container(
            height: double.infinity,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage(
                      'assets/med1.png',
                    ),
                    fit: BoxFit.fill)),
            width: 50,
          ),
          title: Text(
            'Med 1',
            style: GoogleFonts.abhayaLibre(fontSize: 20),
          ),
          subtitle: Container(
            // alignment: Alignment.topLeft,
            // color: Colors.red,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: Text('From: x12345abcd....',
                      style: GoogleFonts.abhayaLibre(fontSize: 16)),
                ),
                Text('June 26, 2023 06:23pm',
                    style: GoogleFonts.abhayaLibre(fontSize: 14))
              ],
            ),
          ),
          // isThreeLine: true,
        ),
      ),
    );
  }
}
