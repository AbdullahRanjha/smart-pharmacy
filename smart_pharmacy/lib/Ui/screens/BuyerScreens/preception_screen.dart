import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:smart_pharmacy/components/Custom_appbar.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class PreceptionScreen extends StatefulWidget {
  const PreceptionScreen({super.key});

  @override
  State<PreceptionScreen> createState() => _PreceptionScreenState();
}

class _PreceptionScreenState extends State<PreceptionScreen> {
  Constants constants = Constants();
  String imagePath = '';
  final ImagePicker _picker = ImagePicker();
  _getFromGallery() async {
    final image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      imagePath = image.path.toString();

      // return Exception('Select your picture');
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 0, left: 10, right: 10),
          child: Container(
            // color: Colors.red,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      CustomAppbar(),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                                image:
                                    AssetImage('assets/preception_image.png'))),
                      ),
                      Text(
                        'Upload Preceptions',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Upload Preceptions and get your medicine of your door step.',
                        style: TextStyle(
                          fontSize: 19,
                        ),
                      ),
                      CustomButton(
                        title: 'Upload Image',
                        radius: 10,
                        backgroundColor: constants.buttonColor,
                        fontSize: 21,
                        onTap: () {},
                        textColor: constants.buttonTextColor,
                        height: MediaQuery.of(context).size.height * 0.06,
                        width: MediaQuery.of(context).size.width * 0.95,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CustomButton(
                        title: 'Gallery',
                        radius: 10,
                        backgroundColor: constants.buttonColor,
                        fontSize: 21,
                        onTap: () {
                          _getFromGallery();
                          print('object');
                        },
                        textColor: constants.buttonTextColor,
                        height: MediaQuery.of(context).size.height * 0.06,
                        width: MediaQuery.of(context).size.width * 0.95,
                      ),

                      Container(
                        height: MediaQuery.of(context).size.height * 0.5,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: FileImage(File(imagePath.toString())))),
                      )

                      //  eImage(File(image.toString())),
                    ],
                  ),
                ),
                Positioned(
                  top: 585,
                  right: 0,
                  left: 0,
                  child: CustomButton(
                    title: 'Buy Medicine',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    width: MediaQuery.of(context).size.width * 0.95,
                    fontSize: 21,
                    onTap: () {},
                    textColor: constants.whiteColor,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
