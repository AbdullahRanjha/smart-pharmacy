import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_offer_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_offer_screen1.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_offer_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_orderScreen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_profile_screen.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/buyer_settings.dart';
// import 'package:smart_pharmacy/Ui/screens/BuyerScreens/offer_screen.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/payment_tracker_page.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/main_loginPage.dart';
import 'package:smart_pharmacy/components/2ndButton.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class EndDrawer extends StatefulWidget {
  const EndDrawer({super.key});

  @override
  State<EndDrawer> createState() => _EndDrawerState();
}

class _EndDrawerState extends State<EndDrawer> {
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      children: [
        Container(
          height: double.infinity,
          color: Colors.grey[400],
          width: MediaQuery.of(context).size.width * 0.35,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.65,
          color: constants.buttonColor,
          height: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: [
                Row(
                  children: [
                    Container(
                      child: CircleAvatar(
                        radius: 45,
                        child: Text(
                          'AR',
                          style: TextStyle(fontSize: 27),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Shahzaib',
                            style: TextStyle(fontSize: 19, color: Colors.white),
                          ),
                          Text(
                            'Buyer',
                            style: TextStyle(fontSize: 17),
                          ),
                        ],
                      ),
                    )
                    // Column()
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Column(
                        children: [
                          SecondButton(
                              title: 'Profile',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(BuyerProfileScreen());
                              },
                              width: double.infinity),
                          SecondButton(
                              title: 'Order',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(BuyerOrderScreen());
                              },
                              width: double.infinity),
                          // SecondButton(
                          //     title: 'Offers',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {
                          //       Get.to(BuyerOfferScreen());
                          //     },
                          //     width: double.infinity),
                          SecondButton(
                              title: 'Offers',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(BuyerOfferScreen());
                              },
                              width: double.infinity),
                          SecondButton(
                              title: 'Track Orders',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(PaymentTrackerScreen());
                              },
                              width: double.infinity),
                          SecondButton(
                              title: 'Settings',
                              radius: 0,
                              borderColor: Colors.white,
                              height: 40,
                              textColor: Colors.white,
                              fontSize: 20,
                              onTap: () {
                                Get.to(BuyerProfileScreen());
                                // Get.to(BuyerOfferScreen());
                              },
                              width: double.infinity),
                          // SecondButton(
                          //     title: 'Help & Supports',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                          // SecondButton(
                          //     title: 'Settings',
                          //     radius: 0,
                          //     borderColor: Colors.white,
                          //     height: 40,
                          //     textColor: Colors.white,
                          //     fontSize: 20,
                          //     onTap: () {},
                          //     width: double.infinity),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 200),
                      // alignment:,
                      child: SecondButton(
                          title: 'Logout',
                          radius: 0,
                          borderColor: Colors.white,
                          height: 40,
                          textColor: Colors.white,
                          fontSize: 20,
                          onTap: () {
                            Get.to(MainLoginPage());
                          },
                          width: double.infinity),
                    )
                  ],
                )
                // DrawerHeader(
                //     decoration: BoxDecoration(color: Colors.grey[350]),
                //     child: Column(
                //       children: [
                //         Container(
                //           child: Text('Name'),
                //         ),
                //         Container(
                //           child: Text('Name'),
                //         ),
                //         Container(
                //           child: Text('Name'),
                //         ),
                //         Container(
                //           child: Text('Name'),
                //         )
                //       ],
                //     ))
              ],
            ),
          ),
        )
      ],
    ));
  }
}
