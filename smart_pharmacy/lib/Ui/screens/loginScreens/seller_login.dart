import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/SellersScreens/seller_main_screen.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class SellerLogin extends StatefulWidget {
  const SellerLogin({super.key});

  @override
  State<SellerLogin> createState() => _SellerLoginState();
}

class _SellerLoginState extends State<SellerLogin> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Constants constants = Constants();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('assets/2ndPicture.jpg'),
                // Container(
                //   height: MediaQuery.of(context).size.height * 0.32,
                //   child: Image.asset(
                //     'assets/2ndPicture.jpg',
                //   ),
                // )
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Seller Login',
                  style: TextStyle(fontSize: 27),
                ),
                SizedBox(
                  height: 20,
                ),
                CustomtextFormField(
                    hintText: 'Enter Email',
                    horizontalPadding: 10,
                    lableText: 'Email',
                    controller: emailController,
                    // textFieldHeight: 0,
                    radius: 10,
                    keyBoardtype: TextInputType.emailAddress,
                    verticalPadding: 10),
                CustomtextFormField(
                    // keyBoardtype: ,
                    keyBoardtype: TextInputType.emailAddress,
                    hintText: 'Enter Password',
                    horizontalPadding: 10,
                    // textFieldHeight: 0,
                    lableText: 'Password',
                    controller: passwordController,
                    radius: 10,
                    verticalPadding: 10),
                SizedBox(
                  height: 10,
                ),
                CustomButton(
                    title: 'Login',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: 23,
                    onTap: () {
                      Get.to(MainScreen());
                    },
                    textColor: constants.whiteColor,
                    width: MediaQuery.of(context).size.width * 0.95),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
