import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/BuyerScreens/main_page.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class BuyerLogin extends StatefulWidget {
  const BuyerLogin({super.key});

  @override
  State<BuyerLogin> createState() => _BuyerLoginState();
}

class _BuyerLoginState extends State<BuyerLogin> {
  Constants constants = Constants();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Image.asset(
                    'assets/9thPicture.jpg',
                    // height: MediaQuery.of(context).size.height * 0.32,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  'Login Buyer',
                  style: TextStyle(fontSize: 26),
                ),
                SizedBox(height: 10),
                CustomtextFormField(
                    hintText: 'Enter Email',
                    // textFieldHeight: 0,
                    keyBoardtype: TextInputType.emailAddress,
                    horizontalPadding: 10,
                    lableText: 'Email',
                    controller: emailController,
                    radius: 10,
                    verticalPadding: 10),
                CustomtextFormField(
                    hintText: 'Enter Password',
                    horizontalPadding: 10,
                    keyBoardtype: TextInputType.emailAddress,
                    lableText: 'Password',
                    // textFieldHeight: 0,
                    controller: passwordController,
                    radius: 10,
                    verticalPadding: 10),
                SizedBox(
                  height: 10,
                ),
                CustomButton(
                    title: 'login',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: 23,
                    onTap: () {
                      Get.to(BuyerHomePage());
                      // ));
                    },
                    textColor: constants.buttonTextColor,
                    width: MediaQuery.of(context).size.width * 0.95),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
