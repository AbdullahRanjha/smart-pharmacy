import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/AdminScreens/main_screen.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/components/textFormField.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class AdminLogin extends StatefulWidget {
  const AdminLogin({super.key});

  @override
  State<AdminLogin> createState() => _AdminLoginState();
}

class _AdminLoginState extends State<AdminLogin> {
  Constants constants = Constants();

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Image.asset(
                  'assets/10Picture.jpg',
                  // height: MediaQuery.of(context).size.height * 0.32,
                ),
              ),
              SizedBox(height: 10),
              Text(
                'Admin Login',
                style: TextStyle(fontSize: 27),
              ),
              SizedBox(height: 10),
              CustomtextFormField(
                  hintText: 'Enter Email',
                  // textFieldHeight: 0,
                  horizontalPadding: 10,
                  lableText: 'Email',
                  keyBoardtype: TextInputType.emailAddress,
                  controller: emailController,
                  radius: 10,
                  verticalPadding: 10),
              CustomtextFormField(
                  hintText: 'Enter Password',
                  // textFieldHeight: 0,
                  horizontalPadding: 10,
                  lableText: 'Password',
                  keyBoardtype: TextInputType.emailAddress,
                  controller: passwordController,
                  radius: 10,
                  verticalPadding: 10),
              SizedBox(
                height: 10,
              ),
              CustomButton(
                  title: 'login',
                  radius: 10,
                  backgroundColor: constants.buttonColor,
                  height: MediaQuery.of(context).size.height * 0.06,
                  fontSize: 25,
                  onTap: () {
                    Get.to(AdminMainScreen());
                  },
                  textColor: constants.buttonTextColor,
                  width: MediaQuery.of(context).size.width * 0.95),
            ],
          ),
        ),
      )),
    );
  }
}
