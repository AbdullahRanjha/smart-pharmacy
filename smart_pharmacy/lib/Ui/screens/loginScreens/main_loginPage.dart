import 'package:flutter/material.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/admin_login.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/buyer_login.dart';
import 'package:smart_pharmacy/Ui/screens/loginScreens/seller_login.dart';
import 'package:smart_pharmacy/components/button.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class MainLoginPage extends StatefulWidget {
  const MainLoginPage({super.key});

  @override
  State<MainLoginPage> createState() => _MainLoginPageState();
}

class _MainLoginPageState extends State<MainLoginPage> {
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // top: false,S
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Image.asset('assets/2ndPicture.jpg'),
                SizedBox(
                  height: 70,
                ),
                CustomButton(
                    title: 'Login as Seller',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: constants.buttontextFontSize,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SellerLogin()));
                    },
                    textColor: constants.whiteColor,
                    width: MediaQuery.of(context).size.width * 0.79),
                SizedBox(
                  height: 20,
                ),
                CustomButton(
                    title: 'Login as Buyers',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: constants.buttontextFontSize,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BuyerLogin()));
                    },
                    textColor: constants.whiteColor,
                    width: MediaQuery.of(context).size.width * 0.79),
                SizedBox(
                  height: 20,
                ),
                CustomButton(
                    title: 'Login as Admin',
                    radius: 10,
                    backgroundColor: constants.buttonColor,
                    height: MediaQuery.of(context).size.height * 0.06,
                    fontSize: constants.buttontextFontSize,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AdminLogin()));
                    },
                    textColor: constants.whiteColor,
                    width: MediaQuery.of(context).size.width * 0.79),
                SizedBox(
                  height: 20,
                ),
               
              ],
            ),
          ),
        ),
      ),
    );
  }
}
