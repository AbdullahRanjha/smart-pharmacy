import 'package:easy_splash_screen/easy_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_pharmacy/Ui/screens/signup/main_signup_page.dart';
import 'package:smart_pharmacy/constants/constant.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Constants constants = Constants();
  @override
  Widget build(BuildContext context) {
    return EasySplashScreen(
        // backgroundColor: constants.buttonColor,
        // backgroundColor: constants.backgroundColor,
        durationInSeconds: 3,
        logoWidth: MediaQuery.of(context).size.width * 0.55,
        showLoader: true,
        navigator: SignupPage(),
        title: Text(
          'Smart Pharmacy',
          style: TextStyle(fontSize: 30),
        ),
        logo: Image.asset(
          'assets/splashPicture.jpg',
        ));
    // return Scaffold(
    //   appBar: AppBar(
    //     automaticallyImplyLeading: false,
    //   ),
    //   body: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: [
    //       Center(
    //         child: Image.asset(
    //           'assets/splashPicture.jpg',
    //           fit: BoxFit.fill,
    //         ),
    //       ),
    //       Text(
    //         'Smart Pharmacy',
    //         style: GoogleFonts.abhayaLibre(
    //             fontSize: 30, fontWeight: FontWeight.normal),
    //       )
    //     ],
    //   ),
    // );
  }
}
